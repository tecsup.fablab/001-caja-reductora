<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="8.7.1">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="yes" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="7" fill="1" visible="yes" active="yes"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="yes" active="yes"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="117" name="Drilling" color="63" fill="1" visible="no" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="fab" urn="urn:adsk.eagle:library:2432493">
<description>&lt;b&gt;Fablab Library&lt;/b&gt;

&lt;p&gt;This library contains many components found on Fablab's inventory.&lt;/p&gt;</description>
<packages>
<package name="PJ-002AH-SMT-TR" urn="urn:adsk.eagle:footprint:2727690/1" locally_modified="yes" library_version="9" library_locally_modified="yes">
<description>&lt;p&gt;Technical specifications:
&lt;ul&gt;
&lt;li&gt;Rated Input Voltage (typ): 24 V
&lt;li&gt;Rated Input Current (max): 5 A
&lt;li&gt;Operating Temperature: -25 to 85 °C
&lt;/ul&gt;
&lt;p&gt;Digikey code: CP-002AHPJCT-ND
&lt;p&gt;
&lt;a href="https://www.cui.com/product/resource/pj-002ah-smt-tr.pdf"&gt;Datasheet&lt;/a&gt;&lt;/p&gt;</description>
<wire x1="-7.4" y1="4.4" x2="-7.4" y2="-4.5" width="0.127" layer="21"/>
<wire x1="-7.4" y1="-4.5" x2="7.4" y2="-4.5" width="0.127" layer="21"/>
<wire x1="7.4" y1="-4.5" x2="7.4" y2="4.5" width="0.127" layer="21"/>
<wire x1="7.4" y1="4.5" x2="-7.4" y2="4.5" width="0.127" layer="21"/>
<wire x1="2.54" y1="0" x2="-7.62" y2="0" width="0.635" layer="21"/>
<smd name="1@2" x="3.7" y="6" dx="2.8" dy="2.4" layer="1" rot="R270"/>
<smd name="3" x="3.7" y="-6" dx="2.8" dy="2.4" layer="1" rot="R270"/>
<smd name="2" x="-2.4" y="-6" dx="2.8" dy="2.4" layer="1" rot="R270"/>
<smd name="1@1" x="-2.4" y="6" dx="2.8" dy="2.4" layer="1" rot="R270"/>
<text x="-6" y="2" size="1.27" layer="25">&gt;NAME</text>
<rectangle x1="2.75" y1="4.75" x2="4.55" y2="6.25" layer="51" rot="R270"/>
<rectangle x1="2.75" y1="-6.25" x2="4.55" y2="-4.75" layer="51" rot="R270"/>
<rectangle x1="-3.25" y1="-6.25" x2="-1.45" y2="-4.75" layer="51" rot="R270"/>
<rectangle x1="-3.35" y1="4.75" x2="-1.55" y2="6.25" layer="51" rot="R270"/>
<hole x="-2.4" y="0" drill="1.7"/>
<hole x="2.1" y="0" drill="1.8"/>
<circle x="-2.4" y="0" radius="0.425" width="0.85" layer="117"/>
<circle x="2.1" y="0" radius="0.45" width="0.9" layer="117"/>
</package>
<package name="3.5MMTERM" urn="urn:adsk.eagle:footprint:2432595/1" library_version="9" library_locally_modified="yes">
<wire x1="-3.4" y1="3.4" x2="-3.4" y2="-2.2" width="0.127" layer="21"/>
<wire x1="-3.4" y1="-2.2" x2="-3.4" y2="-3.6" width="0.127" layer="21"/>
<wire x1="-3.4" y1="-3.6" x2="3.6" y2="-3.6" width="0.127" layer="21"/>
<wire x1="3.6" y1="-3.6" x2="3.6" y2="-2.2" width="0.127" layer="21"/>
<wire x1="3.6" y1="-2.2" x2="3.6" y2="3.4" width="0.127" layer="21"/>
<wire x1="3.6" y1="3.4" x2="-3.4" y2="3.4" width="0.127" layer="21"/>
<wire x1="-3.4" y1="-2.2" x2="3.6" y2="-2.2" width="0.127" layer="21"/>
<pad name="1" x="1.8" y="0" drill="1" diameter="2.1844"/>
<pad name="2" x="-1.7" y="0" drill="1" diameter="2.1844"/>
<text x="3" y="5" size="1.27" layer="25" rot="R180">&gt;NAME</text>
</package>
<package name="ED555DS-2DS" urn="urn:adsk.eagle:footprint:2432596/1" library_version="9" library_locally_modified="yes">
<wire x1="-3.4" y1="3.4" x2="-3.4" y2="-2.2" width="0.127" layer="21"/>
<wire x1="-3.4" y1="-2.2" x2="-3.4" y2="-3.6" width="0.127" layer="21"/>
<wire x1="-3.4" y1="-3.6" x2="3.6" y2="-3.6" width="0.127" layer="21"/>
<wire x1="3.6" y1="-3.6" x2="3.6" y2="-2.2" width="0.127" layer="21"/>
<wire x1="3.6" y1="-2.2" x2="3.6" y2="3.4" width="0.127" layer="21"/>
<wire x1="3.6" y1="3.4" x2="-3.4" y2="3.4" width="0.127" layer="21"/>
<wire x1="-3.4" y1="-2.2" x2="3.6" y2="-2.2" width="0.127" layer="21"/>
<pad name="1" x="1.8" y="0" drill="1.2" diameter="2.1844"/>
<pad name="2" x="-1.7" y="0" drill="1.2" diameter="2.1844"/>
<text x="3" y="5" size="1.27" layer="25" rot="R180">&gt;NAME</text>
</package>
<package name="ED555DS-2DS-DRILL" urn="urn:adsk.eagle:footprint:2593471/2" library_version="9" library_locally_modified="yes">
<wire x1="-3.4" y1="3.4" x2="-3.4" y2="-2.2" width="0.127" layer="21"/>
<wire x1="-3.4" y1="-2.2" x2="-3.4" y2="-3.6" width="0.127" layer="21"/>
<wire x1="-3.4" y1="-3.6" x2="3.6" y2="-3.6" width="0.127" layer="21"/>
<wire x1="3.6" y1="-3.6" x2="3.6" y2="-2.2" width="0.127" layer="21"/>
<wire x1="3.6" y1="-2.2" x2="3.6" y2="3.4" width="0.127" layer="21"/>
<wire x1="3.6" y1="3.4" x2="-3.4" y2="3.4" width="0.127" layer="21"/>
<wire x1="-3.4" y1="-2.2" x2="3.6" y2="-2.2" width="0.127" layer="21"/>
<pad name="1" x="1.8" y="0" drill="1.016" diameter="2.54"/>
<pad name="2" x="-1.7" y="0" drill="1.016" diameter="2.54"/>
<text x="3" y="5" size="1.27" layer="25" rot="R180">&gt;NAME</text>
<circle x="1.8" y="0" radius="0.254" width="0.508" layer="117"/>
<circle x="-1.7" y="0" radius="0.254" width="0.508" layer="117"/>
</package>
<package name="AYZ0102AGRLC" urn="urn:adsk.eagle:footprint:2432594/1" library_version="9" library_locally_modified="yes">
<wire x1="-3.6" y1="-1.5" x2="3.6" y2="-1.5" width="0.127" layer="21"/>
<wire x1="3.6" y1="-1.5" x2="3.6" y2="1.5" width="0.127" layer="21"/>
<wire x1="3.6" y1="1.5" x2="1.5" y2="1.5" width="0.127" layer="21"/>
<wire x1="1.5" y1="1.5" x2="-1.5" y2="1.5" width="0.127" layer="21"/>
<wire x1="-1.5" y1="1.5" x2="-3.6" y2="1.5" width="0.127" layer="21"/>
<wire x1="-3.6" y1="1.5" x2="-3.6" y2="-1.5" width="0.127" layer="21"/>
<wire x1="0" y1="1.6" x2="0" y2="2.5" width="0.127" layer="21"/>
<wire x1="0" y1="2.5" x2="1.5" y2="2.5" width="0.127" layer="21"/>
<wire x1="1.5" y1="2.5" x2="1.5" y2="1.5" width="0.127" layer="21"/>
<smd name="2" x="0" y="-2.3" dx="1" dy="1.2" layer="1"/>
<smd name="3" x="-2.5" y="-2.3" dx="1" dy="1.2" layer="1"/>
<smd name="1" x="2.5" y="-2.3" dx="1" dy="1.2" layer="1"/>
<text x="-2.794" y="2.794" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.302" y="-4.572" size="1.27" layer="27">&gt;VALUE</text>
<hole x="-1.5" y="0.4" drill="0.85"/>
<hole x="1.5" y="0.4" drill="0.85"/>
</package>
<package name="AYZ0102AGRLC_W/_DRILL_HOLES" library_version="9" library_locally_modified="yes">
<wire x1="-3.6" y1="-1.5" x2="3.6" y2="-1.5" width="0.127" layer="21"/>
<wire x1="3.6" y1="-1.5" x2="3.6" y2="1.5" width="0.127" layer="21"/>
<wire x1="3.6" y1="1.5" x2="1.5" y2="1.5" width="0.127" layer="21"/>
<wire x1="1.5" y1="1.5" x2="0" y2="1.5" width="0.127" layer="21"/>
<wire x1="0" y1="1.5" x2="-3.6" y2="1.5" width="0.127" layer="21"/>
<wire x1="-3.6" y1="1.5" x2="-3.6" y2="-1.5" width="0.127" layer="21"/>
<wire x1="0" y1="1.5" x2="0" y2="2.5" width="0.127" layer="21"/>
<wire x1="0" y1="2.5" x2="1.5" y2="2.5" width="0.127" layer="21"/>
<wire x1="1.5" y1="2.5" x2="1.5" y2="1.5" width="0.127" layer="21"/>
<smd name="2" x="0" y="-2.6" dx="1" dy="1.2" layer="1"/>
<smd name="3" x="-2.5" y="-2.6" dx="1" dy="1.2" layer="1"/>
<smd name="1" x="2.5" y="-2.6" dx="1" dy="1.2" layer="1"/>
<text x="-2.794" y="2.794" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.302" y="-4.572" size="1.27" layer="27">&gt;VALUE</text>
<hole x="-1.5" y="0" drill="0.85"/>
<hole x="1.5" y="0" drill="0.85"/>
<circle x="-1.5" y="0" radius="0.2125" width="0.425" layer="117"/>
<circle x="1.5" y="0" radius="0.2125" width="0.425" layer="117"/>
<rectangle x1="-0.25" y1="-2.8" x2="0.25" y2="-1.5" layer="51"/>
<rectangle x1="2.25" y1="-2.8" x2="2.75" y2="-1.5" layer="51"/>
<rectangle x1="-2.75" y1="-2.8" x2="-2.25" y2="-1.5" layer="51"/>
</package>
<package name="ZLDO1117" urn="urn:adsk.eagle:footprint:2727693/1" library_version="9" library_locally_modified="yes">
<description>&lt;b&gt;SOT-223&lt;/b&gt;</description>
<wire x1="3.25" y1="1.75" x2="3.25" y2="-1.75" width="0.2032" layer="21"/>
<wire x1="3.25" y1="-1.75" x2="-3.25" y2="-1.75" width="0.2032" layer="21"/>
<wire x1="-3.25" y1="-1.75" x2="-3.25" y2="1.75" width="0.2032" layer="21"/>
<wire x1="-3.25" y1="1.75" x2="3.25" y2="1.75" width="0.2032" layer="21"/>
<smd name="GND" x="-2.3" y="-3.2" dx="1.2" dy="1.6" layer="1"/>
<smd name="VOUT@2" x="0" y="-3.2" dx="1.2" dy="1.6" layer="1"/>
<smd name="VIN" x="2.3" y="-3.2" dx="1.2" dy="1.6" layer="1"/>
<smd name="VOUT@1" x="0" y="3.2" dx="3.3" dy="1.6" layer="1" thermals="no"/>
<text x="-0.8255" y="4.5085" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.0795" y="-0.1905" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-1.5" y1="1.75" x2="1.5" y2="3.5" layer="51"/>
<rectangle x1="1.95" y1="-3.5" x2="2.65" y2="-1.75" layer="51"/>
<rectangle x1="-0.35" y1="-3.5" x2="0.35" y2="-1.75" layer="51"/>
<rectangle x1="-2.65" y1="-3.5" x2="-1.95" y2="-1.75" layer="51"/>
</package>
<package name="C1206" urn="urn:adsk.eagle:footprint:2626989/1" library_version="9" library_locally_modified="yes">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="C1206FAB" urn="urn:adsk.eagle:footprint:2626988/1" library_version="9" library_locally_modified="yes">
<wire x1="-2.032" y1="1.016" x2="2.032" y2="1.016" width="0.127" layer="21"/>
<wire x1="2.032" y1="1.016" x2="2.032" y2="-1.016" width="0.127" layer="21"/>
<wire x1="2.032" y1="-1.016" x2="-2.032" y2="-1.016" width="0.127" layer="21"/>
<wire x1="-2.032" y1="-1.016" x2="-2.032" y2="1.016" width="0.127" layer="21"/>
<smd name="1" x="-1.651" y="0" dx="1.27" dy="1.905" layer="1"/>
<smd name="2" x="1.651" y="0" dx="1.27" dy="1.905" layer="1"/>
<text x="-1.778" y="1.27" size="1.016" layer="25" ratio="15">&gt;NAME</text>
<text x="-1.778" y="-2.286" size="1.016" layer="27" ratio="15">&gt;VALUE</text>
</package>
<package name="C2220" urn="urn:adsk.eagle:footprint:2627001/1" library_version="9" library_locally_modified="yes">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-3.743" y1="2.253" x2="3.743" y2="2.253" width="0.0508" layer="39"/>
<wire x1="3.743" y1="-2.253" x2="-3.743" y2="-2.253" width="0.0508" layer="39"/>
<wire x1="-3.743" y1="-2.253" x2="-3.743" y2="2.253" width="0.0508" layer="39"/>
<wire x1="3.743" y1="2.253" x2="3.743" y2="-2.253" width="0.0508" layer="39"/>
<smd name="1" x="-2.794" y="0" dx="2.032" dy="5.334" layer="1"/>
<smd name="2" x="2.794" y="0" dx="2.032" dy="5.334" layer="1"/>
<text x="-2.54" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.9718" y1="-0.8509" x2="-2.2217" y2="0.8491" layer="51"/>
<rectangle x1="2.2217" y1="-0.8491" x2="2.9718" y2="0.8509" layer="51"/>
</package>
<package name="TQFP32-08THIN" urn="urn:adsk.eagle:footprint:2432560/1" library_version="9" library_locally_modified="yes">
<wire x1="3.505" y1="3.505" x2="3.505" y2="-3.505" width="0.1524" layer="21"/>
<wire x1="3.505" y1="-3.505" x2="-3.505" y2="-3.505" width="0.1524" layer="21"/>
<wire x1="-3.505" y1="-3.505" x2="-3.505" y2="3.15" width="0.1524" layer="21"/>
<wire x1="-3.15" y1="3.505" x2="3.505" y2="3.505" width="0.1524" layer="21"/>
<wire x1="-3.15" y1="3.505" x2="-3.505" y2="3.15" width="0.1524" layer="21"/>
<circle x="-2.7432" y="2.7432" radius="0.3592" width="0.1524" layer="21"/>
<smd name="1" x="-4.3688" y="2.8" dx="1.1176" dy="0.3556" layer="1"/>
<smd name="2" x="-4.2926" y="2" dx="1.27" dy="0.3556" layer="1"/>
<smd name="3" x="-4.2926" y="1.2" dx="1.27" dy="0.3556" layer="1"/>
<smd name="4" x="-4.2926" y="0.4" dx="1.27" dy="0.3556" layer="1"/>
<smd name="5" x="-4.2926" y="-0.4" dx="1.27" dy="0.3556" layer="1"/>
<smd name="6" x="-4.2926" y="-1.2" dx="1.27" dy="0.3556" layer="1"/>
<smd name="7" x="-4.2926" y="-2" dx="1.27" dy="0.3556" layer="1"/>
<smd name="8" x="-4.3688" y="-2.8" dx="1.1176" dy="0.3556" layer="1"/>
<smd name="9" x="-2.8" y="-4.3688" dx="0.3556" dy="1.1176" layer="1"/>
<smd name="10" x="-2" y="-4.2926" dx="0.3556" dy="1.27" layer="1"/>
<smd name="11" x="-1.2" y="-4.2926" dx="0.3504" dy="1.27" layer="1"/>
<smd name="12" x="-0.4" y="-4.2926" dx="0.3504" dy="1.27" layer="1"/>
<smd name="13" x="0.4" y="-4.2926" dx="0.3504" dy="1.27" layer="1"/>
<smd name="14" x="1.2" y="-4.2926" dx="0.3556" dy="1.27" layer="1"/>
<smd name="15" x="2" y="-4.2926" dx="0.3556" dy="1.27" layer="1"/>
<smd name="16" x="2.8" y="-4.3688" dx="0.3556" dy="1.1176" layer="1"/>
<smd name="17" x="4.3688" y="-2.8" dx="1.1176" dy="0.3556" layer="1"/>
<smd name="18" x="4.2926" y="-2" dx="1.27" dy="0.3556" layer="1"/>
<smd name="19" x="4.2926" y="-1.2" dx="1.27" dy="0.3556" layer="1"/>
<smd name="20" x="4.2926" y="-0.4" dx="1.27" dy="0.3556" layer="1"/>
<smd name="21" x="4.2926" y="0.4" dx="1.27" dy="0.3556" layer="1"/>
<smd name="22" x="4.2926" y="1.2" dx="1.27" dy="0.3556" layer="1"/>
<smd name="23" x="4.2926" y="2" dx="1.27" dy="0.3556" layer="1"/>
<smd name="24" x="4.3688" y="2.8" dx="1.1176" dy="0.3556" layer="1"/>
<smd name="25" x="2.8" y="4.3688" dx="0.3556" dy="1.1176" layer="1"/>
<smd name="26" x="2" y="4.2926" dx="0.3556" dy="1.27" layer="1"/>
<smd name="27" x="1.2" y="4.2926" dx="0.3556" dy="1.27" layer="1"/>
<smd name="28" x="0.4" y="4.2926" dx="0.3504" dy="1.27" layer="1"/>
<smd name="29" x="-0.4" y="4.2926" dx="0.3504" dy="1.27" layer="1"/>
<smd name="30" x="-1.2" y="4.2926" dx="0.3504" dy="1.27" layer="1"/>
<smd name="31" x="-2" y="4.2926" dx="0.3556" dy="1.27" layer="1"/>
<smd name="32" x="-2.8" y="4.3688" dx="0.3556" dy="1.1176" layer="1"/>
<text x="-2.7686" y="5.08" size="0.8128" layer="25">&gt;NAME</text>
<text x="-3.0226" y="-1.27" size="0.8128" layer="27">&gt;VALUE</text>
<rectangle x1="-4.5466" y1="2.5714" x2="-3.556" y2="3.0286" layer="51"/>
<rectangle x1="-4.5466" y1="1.7714" x2="-3.556" y2="2.2286" layer="51"/>
<rectangle x1="-4.5466" y1="0.9714" x2="-3.556" y2="1.4286" layer="51"/>
<rectangle x1="-4.5466" y1="0.1714" x2="-3.556" y2="0.6286" layer="51"/>
<rectangle x1="-4.5466" y1="-0.6286" x2="-3.556" y2="-0.1714" layer="51"/>
<rectangle x1="-4.5466" y1="-1.4286" x2="-3.556" y2="-0.9714" layer="51"/>
<rectangle x1="-4.5466" y1="-2.2286" x2="-3.556" y2="-1.7714" layer="51"/>
<rectangle x1="-4.5466" y1="-3.0286" x2="-3.556" y2="-2.5714" layer="51"/>
<rectangle x1="-3.0286" y1="-4.5466" x2="-2.5714" y2="-3.556" layer="51"/>
<rectangle x1="-2.2286" y1="-4.5466" x2="-1.7714" y2="-3.556" layer="51"/>
<rectangle x1="-1.4286" y1="-4.5466" x2="-0.9714" y2="-3.556" layer="51"/>
<rectangle x1="-0.6286" y1="-4.5466" x2="-0.1714" y2="-3.556" layer="51"/>
<rectangle x1="0.1714" y1="-4.5466" x2="0.6286" y2="-3.556" layer="51"/>
<rectangle x1="0.9714" y1="-4.5466" x2="1.4286" y2="-3.556" layer="51"/>
<rectangle x1="1.7714" y1="-4.5466" x2="2.2286" y2="-3.556" layer="51"/>
<rectangle x1="2.5714" y1="-4.5466" x2="3.0286" y2="-3.556" layer="51"/>
<rectangle x1="3.556" y1="-3.0286" x2="4.5466" y2="-2.5714" layer="51"/>
<rectangle x1="3.556" y1="-2.2286" x2="4.5466" y2="-1.7714" layer="51"/>
<rectangle x1="3.556" y1="-1.4286" x2="4.5466" y2="-0.9714" layer="51"/>
<rectangle x1="3.556" y1="-0.6286" x2="4.5466" y2="-0.1714" layer="51"/>
<rectangle x1="3.556" y1="0.1714" x2="4.5466" y2="0.6286" layer="51"/>
<rectangle x1="3.556" y1="0.9714" x2="4.5466" y2="1.4286" layer="51"/>
<rectangle x1="3.556" y1="1.7714" x2="4.5466" y2="2.2286" layer="51"/>
<rectangle x1="3.556" y1="2.5714" x2="4.5466" y2="3.0286" layer="51"/>
<rectangle x1="2.5714" y1="3.556" x2="3.0286" y2="4.5466" layer="51"/>
<rectangle x1="1.7714" y1="3.556" x2="2.2286" y2="4.5466" layer="51"/>
<rectangle x1="0.9714" y1="3.556" x2="1.4286" y2="4.5466" layer="51"/>
<rectangle x1="0.1714" y1="3.556" x2="0.6286" y2="4.5466" layer="51"/>
<rectangle x1="-0.6286" y1="3.556" x2="-0.1714" y2="4.5466" layer="51"/>
<rectangle x1="-1.4286" y1="3.556" x2="-0.9714" y2="4.5466" layer="51"/>
<rectangle x1="-2.2286" y1="3.556" x2="-1.7714" y2="4.5466" layer="51"/>
<rectangle x1="-3.0286" y1="3.556" x2="-2.5714" y2="4.5466" layer="51"/>
</package>
<package name="2X03SMD" urn="urn:adsk.eagle:footprint:2432541/1" library_version="9" library_locally_modified="yes">
<smd name="1" x="-2.54" y="2.54" dx="2.54" dy="1.27" layer="1"/>
<smd name="3" x="-2.54" y="0" dx="2.54" dy="1.27" layer="1"/>
<smd name="5" x="-2.54" y="-2.54" dx="2.54" dy="1.27" layer="1"/>
<smd name="2" x="2.92" y="2.54" dx="2.54" dy="1.27" layer="1"/>
<smd name="4" x="2.92" y="0" dx="2.54" dy="1.27" layer="1"/>
<smd name="6" x="2.92" y="-2.54" dx="2.54" dy="1.27" layer="1"/>
<text x="-5.08" y="2.54" size="1.27" layer="27">1</text>
<text x="-3.81" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.81" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="2X03" urn="urn:adsk.eagle:footprint:2432547/1" library_version="9" library_locally_modified="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-3.81" y1="-1.905" x2="-3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-2.54" x2="-1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="-0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-2.54" x2="1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-1.905" x2="-3.81" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="1.905" x2="-3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="2.54" x2="-1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="2.54" x2="-1.27" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="2.54" x2="0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="2.54" x2="1.27" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.905" x2="1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-2.54" x2="0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-2.54" x2="-1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.905" x2="1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-2.54" x2="3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.905" x2="1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="2.54" x2="3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="2.54" x2="3.81" y2="1.905" width="0.1524" layer="21"/>
<wire x1="3.81" y1="1.905" x2="3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-2.54" x2="3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-2.921" x2="-1.905" y2="-2.921" width="0.127" layer="21"/>
<pad name="1" x="-2.54" y="-1.27" drill="1.016" shape="square"/>
<pad name="2" x="-2.54" y="1.27" drill="1.016" shape="octagon"/>
<pad name="3" x="0" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="4" x="0" y="1.27" drill="1.016" shape="octagon"/>
<pad name="5" x="2.54" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="6" x="2.54" y="1.27" drill="1.016" shape="octagon"/>
<text x="-3.81" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-4.445" size="1.27" layer="27">&gt;VALUE</text>
<text x="-5.08" y="-2.54" size="1.27" layer="21">1</text>
<rectangle x1="-2.794" y1="-1.524" x2="-2.286" y2="-1.016" layer="51"/>
<rectangle x1="-2.794" y1="1.016" x2="-2.286" y2="1.524" layer="51"/>
<rectangle x1="-0.254" y1="1.016" x2="0.254" y2="1.524" layer="51"/>
<rectangle x1="-0.254" y1="-1.524" x2="0.254" y2="-1.016" layer="51"/>
<rectangle x1="2.286" y1="1.016" x2="2.794" y2="1.524" layer="51"/>
<rectangle x1="2.286" y1="-1.524" x2="2.794" y2="-1.016" layer="51"/>
</package>
<package name="R1206" urn="urn:adsk.eagle:footprint:2432565/1" library_version="9" library_locally_modified="yes">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="0.9525" y1="-0.8128" x2="-0.9652" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="0.9525" y1="0.8128" x2="-0.9652" y2="0.8128" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="2" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="1" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6891" y1="-0.8763" x2="-0.9525" y2="0.8763" layer="51"/>
<rectangle x1="0.9525" y1="-0.8763" x2="1.6891" y2="0.8763" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R1206W" urn="urn:adsk.eagle:footprint:2432566/1" library_version="9" library_locally_modified="yes">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R1206FAB" urn="urn:adsk.eagle:footprint:2432569/1" library_version="9" library_locally_modified="yes">
<wire x1="-2.032" y1="1.016" x2="2.032" y2="1.016" width="0.127" layer="21"/>
<wire x1="2.032" y1="1.016" x2="2.032" y2="-1.016" width="0.127" layer="21"/>
<wire x1="2.032" y1="-1.016" x2="-2.032" y2="-1.016" width="0.127" layer="21"/>
<wire x1="-2.032" y1="-1.016" x2="-2.032" y2="1.016" width="0.127" layer="21"/>
<smd name="1" x="-1.651" y="0" dx="1.27" dy="1.905" layer="1"/>
<smd name="2" x="1.651" y="0" dx="1.27" dy="1.905" layer="1"/>
<text x="-1.778" y="1.27" size="1.016" layer="25" ratio="15">&gt;NAME</text>
<text x="-1.778" y="-2.286" size="1.016" layer="27" ratio="15">&gt;VALUE</text>
</package>
<package name="CSRN2010" urn="urn:adsk.eagle:footprint:2627030/2" library_version="9" library_locally_modified="yes">
<description>&lt;p&gt;Resistence: 0.250 Ohm

&lt;p&gt;Technical specifications:
&lt;ul&gt;
&lt;li&gt;Power rating @ 70°C: 1 W
&lt;li&gt;Dielectric withstanding voltage: 200 V
&lt;/ul&gt;

&lt;p&gt;Manufacturer: Stackpole Electronics Inc.
&lt;p&gt;Digikey code: CSRN2010FKR250CT-ND
&lt;p&gt;&lt;a href="https://www.seielect.com/Catalog/SEI-CSR_CSRN.pdf"&gt;Datasheet&lt;/a&gt;</description>
<smd name="1" x="-2.5019" y="0" dx="2.4892" dy="1.397" layer="1" rot="R90"/>
<smd name="2" x="2.5019" y="0" dx="2.4892" dy="1.397" layer="1" rot="R90"/>
<wire x1="-2.5019" y1="1.27" x2="-2.5019" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-2.5019" y1="-1.27" x2="2.5019" y2="-1.27" width="0.127" layer="21"/>
<wire x1="2.5019" y1="-1.27" x2="2.5019" y2="1.27" width="0.127" layer="21"/>
<wire x1="2.5019" y1="1.27" x2="-2.5019" y2="1.27" width="0.127" layer="21"/>
<text x="-3.302" y="1.524" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.302" y="-2.794" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="1X06SMD" urn="urn:adsk.eagle:footprint:2432571/1" library_version="9" library_locally_modified="yes">
<smd name="1" x="0" y="6.35" dx="2.54" dy="1.27" layer="1"/>
<smd name="2" x="0" y="3.81" dx="2.54" dy="1.27" layer="1"/>
<smd name="3" x="0" y="1.27" dx="2.54" dy="1.27" layer="1"/>
<smd name="4" x="0" y="-1.27" dx="2.54" dy="1.27" layer="1"/>
<smd name="5" x="0" y="-3.81" dx="2.54" dy="1.27" layer="1"/>
<smd name="6" x="0" y="-6.35" dx="2.54" dy="1.27" layer="1"/>
<text x="-1.905" y="-3.175" size="1.27" layer="25" rot="R90">&gt;NAME</text>
</package>
<package name="LED1206" urn="urn:adsk.eagle:footprint:2432575/1" library_version="9" library_locally_modified="yes">
<description>LED 1206 pads (standard pattern)</description>
<wire x1="0.9525" y1="-0.8128" x2="-0.9652" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="0.9525" y1="0.8128" x2="-0.9652" y2="0.8128" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="2" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="1" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6891" y1="-0.8763" x2="-0.9525" y2="0.8763" layer="51"/>
<rectangle x1="0.9525" y1="-0.8763" x2="1.6891" y2="0.8763" layer="51"/>
</package>
<package name="LED1206FAB" urn="urn:adsk.eagle:footprint:2432574/1" library_version="9" library_locally_modified="yes">
<description>LED1206 FAB style (smaller pads to allow trace between)</description>
<wire x1="-2.032" y1="1.016" x2="2.032" y2="1.016" width="0.127" layer="21"/>
<wire x1="2.032" y1="1.016" x2="2.032" y2="-1.016" width="0.127" layer="21"/>
<wire x1="2.032" y1="-1.016" x2="-2.032" y2="-1.016" width="0.127" layer="21"/>
<wire x1="-2.032" y1="-1.016" x2="-2.032" y2="1.016" width="0.127" layer="21"/>
<smd name="1" x="-1.651" y="0" dx="1.27" dy="1.905" layer="1"/>
<smd name="2" x="1.651" y="0" dx="1.27" dy="1.905" layer="1"/>
<text x="-1.778" y="1.27" size="1.016" layer="25" ratio="15">&gt;NAME</text>
<text x="-1.778" y="-2.286" size="1.016" layer="27" ratio="15">&gt;VALUE</text>
</package>
<package name="5MM" urn="urn:adsk.eagle:footprint:2432576/1" library_version="9" library_locally_modified="yes">
<description>5mm round through hole part.</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="21" curve="-286.260205" cap="flat"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<pad name="IN" x="-1.27" y="0" drill="0.8128" diameter="1.4224"/>
<pad name="OUT" x="1.27" y="0" drill="0.8128" diameter="1.4224"/>
<text x="3.175" y="0.5334" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.2004" y="-1.8034" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="PJ1-023-SMT-TR" urn="urn:adsk.eagle:footprint:2727691/1" library_version="9" library_locally_modified="yes">
<description>&lt;p&gt;Technical specifications:
&lt;ul&gt;
&lt;li&gt;Rated Input Voltage (typ): 24 V
&lt;li&gt;Rated Input Current (max): 2.5 A
&lt;li&gt;Operating Temperature: -25 to 85 °C
&lt;/ul&gt;
&lt;p&gt;Digikey code: CP1-023PJCT-ND
&lt;p&gt;
&lt;a href="https://www.cui.com/product/resource/pj1-023-smt-tr.pdf"&gt;Datasheet&lt;/a&gt;&lt;/p&gt;</description>
<wire x1="0" y1="-2.5" x2="0" y2="0" width="0.127" layer="21"/>
<wire x1="0" y1="0" x2="0" y2="2.5" width="0.127" layer="21"/>
<wire x1="0" y1="2.5" x2="2" y2="2.5" width="0.127" layer="21"/>
<wire x1="2" y1="2.5" x2="2" y2="2.8" width="0.127" layer="21"/>
<wire x1="2" y1="2.8" x2="6" y2="2.8" width="0.127" layer="21"/>
<wire x1="6" y1="2.8" x2="6" y2="2.5" width="0.127" layer="21"/>
<wire x1="6" y1="2.5" x2="11" y2="2.5" width="0.127" layer="21"/>
<wire x1="11" y1="2.5" x2="11" y2="-2.5" width="0.127" layer="21"/>
<wire x1="0" y1="-2.5" x2="2" y2="-2.5" width="0.127" layer="21"/>
<wire x1="2" y1="-2.5" x2="2" y2="-2.8" width="0.127" layer="21"/>
<wire x1="2" y1="-2.8" x2="6" y2="-2.8" width="0.127" layer="21"/>
<wire x1="6" y1="-2.8" x2="6" y2="-2.5" width="0.127" layer="21"/>
<wire x1="6" y1="-2.5" x2="11" y2="-2.5" width="0.127" layer="21"/>
<wire x1="0" y1="0" x2="8.6" y2="0" width="0.6" layer="21"/>
<smd name="SHIELD@1" x="3.5" y="3.5" dx="3.5" dy="1.5" layer="1"/>
<smd name="SHIELD@2" x="4.5" y="-3.5" dx="3.5" dy="1.5" layer="1"/>
<smd name="PIN3" x="10" y="3.65" dx="3.5" dy="2.3" layer="1"/>
<smd name="PIN2" x="9" y="-3.65" dx="3.5" dy="2.3" layer="1"/>
<smd name="PIN1" x="12" y="0" dx="2" dy="2.5" layer="1"/>
<hole x="3" y="0" drill="1.6"/>
<hole x="8.5" y="0" drill="1.6"/>
<circle x="3" y="0" radius="0.4" width="0.8" layer="117"/>
<circle x="8.5" y="0" radius="0.4" width="0.8" layer="117"/>
<rectangle x1="2.5" y1="2.8" x2="4.5" y2="3.55" layer="51"/>
<rectangle x1="3.5" y1="-3.55" x2="5.5" y2="-2.8" layer="51"/>
<rectangle x1="9" y1="2.5" x2="11" y2="4.1" layer="51"/>
<rectangle x1="8" y1="-4.1" x2="10" y2="-2.5" layer="51"/>
<rectangle x1="11" y1="-0.5" x2="12.5" y2="0.5" layer="51"/>
<text x="0" y="5.08" size="1.27" layer="25">&gt;NAME</text>
</package>
<package name="SOIC8_PAD" urn="urn:adsk.eagle:footprint:2626990/1" library_version="9" library_locally_modified="yes">
<description>&lt;B&gt;Wide Plastic Gull Wing Small Outline Package&lt;/B&gt;</description>
<wire x1="-2.6" y1="2.25" x2="-2.35" y2="2.5" width="0.1524" layer="21" curve="-90" cap="flat"/>
<wire x1="2.35" y1="2.5" x2="2.6" y2="2.25" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.35" y1="-2.5" x2="2.6" y2="-2.25" width="0.1524" layer="21" curve="90" cap="flat"/>
<wire x1="-2.6" y1="-2.25" x2="-2.35" y2="-2.5" width="0.1524" layer="21" curve="90" cap="flat"/>
<wire x1="2.36" y1="-2.5" x2="-2.34" y2="-2.5" width="0.1524" layer="51"/>
<wire x1="-2.34" y1="2.5" x2="2.36" y2="2.5" width="0.1524" layer="51"/>
<wire x1="-2.21" y1="2.5" x2="-2.34" y2="2.5" width="0.1524" layer="21"/>
<wire x1="-1.59" y1="2.5" x2="-0.95" y2="2.5" width="0.1524" layer="21"/>
<wire x1="-0.32" y1="2.5" x2="0.32" y2="2.5" width="0.1524" layer="21"/>
<wire x1="0.95" y1="2.5" x2="1.59" y2="2.5" width="0.1524" layer="21"/>
<wire x1="2.21" y1="2.5" x2="2.36" y2="2.5" width="0.1524" layer="21"/>
<wire x1="2.2" y1="-2.5" x2="2.33" y2="-2.5" width="0.1524" layer="21"/>
<wire x1="1.59" y1="-2.5" x2="0.94" y2="-2.5" width="0.1524" layer="21"/>
<wire x1="0.32" y1="-2.5" x2="-0.33" y2="-2.5" width="0.1524" layer="21"/>
<wire x1="-0.95" y1="-2.5" x2="-1.59" y2="-2.5" width="0.1524" layer="21"/>
<wire x1="-2.21" y1="-2.5" x2="-2.34" y2="-2.5" width="0.1524" layer="21"/>
<wire x1="-2.6" y1="2.25" x2="-2.6" y2="-2.24" width="0.1524" layer="21"/>
<wire x1="2.6" y1="-2.25" x2="2.6" y2="2.25" width="0.1524" layer="21"/>
<circle x="-1.42" y="-1.115" radius="0.5" width="0.0508" layer="21"/>
<smd name="1" x="-1.905" y="-3.154" dx="0.5" dy="2.2" layer="1"/>
<smd name="2" x="-0.645" y="-3.154" dx="0.5" dy="2.2" layer="1"/>
<smd name="3" x="0.625" y="-3.154" dx="0.5" dy="2.2" layer="1"/>
<smd name="4" x="1.895" y="-3.154" dx="0.5" dy="2.2" layer="1"/>
<smd name="8" x="-1.905" y="3.154" dx="0.5" dy="2.2" layer="1"/>
<smd name="7" x="-0.635" y="3.154" dx="0.5" dy="2.2" layer="1"/>
<smd name="6" x="0.635" y="3.154" dx="0.5" dy="2.2" layer="1"/>
<smd name="5" x="1.905" y="3.154" dx="0.5" dy="2.2" layer="1"/>
<smd name="GND" x="0" y="0" dx="3.8862" dy="2.9972" layer="1"/>
<text x="-2.8575" y="-2.159" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="4.064" y="-2.159" size="1.27" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.08" y1="2.5" x2="-1.73" y2="3.4" layer="51"/>
<rectangle x1="-0.81" y1="2.5" x2="-0.46" y2="3.4" layer="51"/>
<rectangle x1="0.46" y1="2.5" x2="0.81" y2="3.4" layer="51"/>
<rectangle x1="1.73" y1="2.5" x2="2.08" y2="3.4" layer="51"/>
<rectangle x1="1.72" y1="-3.4" x2="2.07" y2="-2.5" layer="51"/>
<rectangle x1="0.45" y1="-3.4" x2="0.8" y2="-2.5" layer="51"/>
<rectangle x1="-0.82" y1="-3.4" x2="-0.47" y2="-2.5" layer="51"/>
<rectangle x1="-2.08" y1="-3.4" x2="-1.73" y2="-2.5" layer="51"/>
</package>
<package name="1X04SMD" urn="urn:adsk.eagle:footprint:2593470/1" library_version="9" library_locally_modified="yes">
<smd name="1" x="0" y="3.81" dx="2.54" dy="1.27" layer="1"/>
<smd name="2" x="0" y="1.27" dx="2.54" dy="1.27" layer="1"/>
<smd name="3" x="0" y="-1.27" dx="2.54" dy="1.27" layer="1"/>
<smd name="4" x="0" y="-3.81" dx="2.54" dy="1.27" layer="1"/>
<text x="-1.905" y="-3.175" size="1.27" layer="25" rot="R90">&gt;NAME</text>
</package>
<package name="CRYSTAL_PTH" urn="urn:adsk.eagle:footprint:26549/1" locally_modified="yes" library_version="9" library_locally_modified="yes">
<description>&lt;B&gt;CRYSTAL&lt;/B&gt;</description>
<wire x1="-3.429" y1="-2.286" x2="3.429" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.286" x2="-3.429" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.778" x2="3.429" y2="-1.778" width="0.0508" layer="21"/>
<wire x1="3.429" y1="1.778" x2="-3.429" y2="1.778" width="0.0508" layer="21"/>
<wire x1="3.429" y1="1.778" x2="3.429" y2="-1.778" width="0.0508" layer="21" curve="-180"/>
<wire x1="3.429" y1="2.286" x2="3.429" y2="-2.286" width="0.1524" layer="21" curve="-180"/>
<wire x1="-3.429" y1="2.286" x2="-3.429" y2="-2.286" width="0.1524" layer="21" curve="180"/>
<wire x1="-3.429" y1="1.778" x2="-3.429" y2="-1.778" width="0.0508" layer="21" curve="180"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" diameter="1.8288" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="0.8128" diameter="1.8288" rot="R90"/>
<text x="-5.08" y="-3.937" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-5.08" y="2.667" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<circle x="-2.54" y="0" radius="0.2032" width="0.4064" layer="117"/>
<circle x="2.54" y="0" radius="0.2032" width="0.4064" layer="117"/>
</package>
<package name="B3SN-3112P" urn="urn:adsk.eagle:footprint:2727692/1" locally_modified="yes" library_version="9" library_locally_modified="yes">
<description>&lt;p&gt;Digikey code: SW262CT-ND

&lt;p&gt;&lt;a href="http://omronfs.omron.com/en_US/ecb/products/pdf/en-b3sn.pdf"&gt;Datasheet&lt;/a&gt;</description>
<wire x1="3.302" y1="-0.762" x2="3.048" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-0.762" x2="3.302" y2="0.762" width="0.1524" layer="21"/>
<wire x1="3.048" y1="0.762" x2="3.302" y2="0.762" width="0.1524" layer="21"/>
<wire x1="3.048" y1="1.016" x2="3.048" y2="2.54" width="0.1524" layer="51"/>
<wire x1="-3.302" y1="0.762" x2="-3.048" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="0.762" x2="-3.302" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-3.048" y1="-0.762" x2="-3.302" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="3.048" y1="2.54" x2="2.54" y2="3.048" width="0.1524" layer="51"/>
<wire x1="2.54" y1="-3.048" x2="3.048" y2="-2.54" width="0.1524" layer="51"/>
<wire x1="3.048" y1="-2.54" x2="3.048" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="3.048" x2="-3.048" y2="2.54" width="0.1524" layer="51"/>
<wire x1="-3.048" y1="2.54" x2="-3.048" y2="1.016" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="-3.048" x2="-3.048" y2="-2.54" width="0.1524" layer="51"/>
<wire x1="-3.048" y1="-2.54" x2="-3.048" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.0508" layer="51"/>
<wire x1="1.27" y1="-1.27" x2="-1.27" y2="-1.27" width="0.0508" layer="51"/>
<wire x1="1.27" y1="-1.27" x2="1.27" y2="1.27" width="0.0508" layer="51"/>
<wire x1="-1.27" y1="1.27" x2="1.27" y2="1.27" width="0.0508" layer="51"/>
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="2.794" width="0.0508" layer="21"/>
<wire x1="1.27" y1="2.794" x2="-1.27" y2="2.794" width="0.0508" layer="21"/>
<wire x1="1.27" y1="2.794" x2="1.27" y2="3.048" width="0.0508" layer="21"/>
<wire x1="1.143" y1="-2.794" x2="-1.27" y2="-2.794" width="0.0508" layer="21"/>
<wire x1="1.143" y1="-2.794" x2="1.143" y2="-3.048" width="0.0508" layer="21"/>
<wire x1="-1.27" y1="-2.794" x2="-1.27" y2="-3.048" width="0.0508" layer="21"/>
<wire x1="2.54" y1="-3.048" x2="2.159" y2="-3.048" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="-3.048" x2="-2.159" y2="-3.048" width="0.1524" layer="51"/>
<wire x1="-2.159" y1="-3.048" x2="-1.27" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="3.048" x2="-2.159" y2="3.048" width="0.1524" layer="51"/>
<wire x1="2.54" y1="3.048" x2="2.159" y2="3.048" width="0.1524" layer="51"/>
<wire x1="2.159" y1="3.048" x2="1.27" y2="3.048" width="0.1524" layer="21"/>
<wire x1="1.27" y1="3.048" x2="-1.27" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="3.048" x2="-2.159" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-3.048" x2="1.143" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-3.048" x2="2.159" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="3.048" y1="-0.762" x2="3.048" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="3.048" y1="0.762" x2="3.048" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-3.048" y1="-0.762" x2="-3.048" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-3.048" y1="0.762" x2="-3.048" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-2.159" x2="1.27" y2="-2.159" width="0.1524" layer="51"/>
<wire x1="1.27" y1="2.286" x2="-1.27" y2="2.286" width="0.1524" layer="51"/>
<wire x1="-2.413" y1="1.27" x2="-2.413" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-2.413" y1="-0.508" x2="-2.413" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-2.413" y1="-0.508" x2="-2.2" y2="0.2" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="1.778" width="0.1524" layer="21"/>
<circle x="-2.159" y="-2.159" radius="0.508" width="0.1524" layer="51"/>
<circle x="2.159" y="-2.032" radius="0.508" width="0.1524" layer="51"/>
<circle x="2.159" y="2.159" radius="0.508" width="0.1524" layer="51"/>
<circle x="-2.159" y="2.159" radius="0.508" width="0.1524" layer="51"/>
<circle x="0" y="0" radius="0.635" width="0.0508" layer="51"/>
<circle x="0" y="0" radius="0.254" width="0.1524" layer="21"/>
<smd name="4" x="-3.375" y="2" dx="2.75" dy="1.4" layer="1"/>
<smd name="3" x="3.375" y="2" dx="2.75" dy="1.4" layer="1"/>
<smd name="2" x="-3.375" y="-2" dx="2.75" dy="1.4" layer="1"/>
<smd name="1" x="3.375" y="-2" dx="2.75" dy="1.4" layer="1"/>
<smd name="GND" x="3.375" y="0" dx="2.75" dy="1.4" layer="1"/>
<text x="-3.048" y="3.683" size="1.27" layer="25" ratio="10">&gt;NAME</text>
</package>
</packages>
<packages3d>
<package3d name="3.5MMTERM" urn="urn:adsk.eagle:package:2432654/1" type="box" library_version="9" library_locally_modified="yes">
<packageinstances>
<packageinstance name="3.5MMTERM"/>
</packageinstances>
</package3d>
<package3d name="ED555DS-2DS" urn="urn:adsk.eagle:package:2432655/1" type="box" library_version="9" library_locally_modified="yes">
<packageinstances>
<packageinstance name="ED555DS-2DS"/>
</packageinstances>
</package3d>
<package3d name="ED555DS-2DS-DRILL" urn="urn:adsk.eagle:package:2593473/2" type="box" library_version="9" library_locally_modified="yes">
<packageinstances>
<packageinstance name="ED555DS-2DS-DRILL"/>
</packageinstances>
</package3d>
<package3d name="AYZ0102AGRLC" urn="urn:adsk.eagle:package:2432653/1" type="box" library_version="9" library_locally_modified="yes">
<packageinstances>
<packageinstance name="AYZ0102AGRLC"/>
</packageinstances>
</package3d>
<package3d name="ZLDO1117" urn="urn:adsk.eagle:package:2727698/1" type="box" library_version="9" library_locally_modified="yes">
<description>&lt;b&gt;SOT-223&lt;/b&gt;</description>
<packageinstances>
<packageinstance name="ZLDO1117"/>
</packageinstances>
</package3d>
<package3d name="C1206" urn="urn:adsk.eagle:package:2432626/3" type="model" library_version="9" library_locally_modified="yes">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<packageinstances>
<packageinstance name="C1206"/>
</packageinstances>
</package3d>
<package3d name="C1206FAB" urn="urn:adsk.eagle:package:2432629/3" type="model" library_version="9" library_locally_modified="yes">
<packageinstances>
<packageinstance name="C1206FAB"/>
</packageinstances>
</package3d>
<package3d name="C2220" urn="urn:adsk.eagle:package:2627042/1" type="box" library_version="9" library_locally_modified="yes">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<packageinstances>
<packageinstance name="C2220"/>
</packageinstances>
</package3d>
<package3d name="TQFP32-08THIN" urn="urn:adsk.eagle:package:2432619/1" type="box" library_version="9" library_locally_modified="yes">
<packageinstances>
<packageinstance name="TQFP32-08THIN"/>
</packageinstances>
</package3d>
<package3d name="2X03SMD" urn="urn:adsk.eagle:package:2432609/2" type="model" library_version="9" library_locally_modified="yes">
<packageinstances>
<packageinstance name="2X03SMD"/>
</packageinstances>
</package3d>
<package3d name="2X03" urn="urn:adsk.eagle:package:2432603/1" type="box" library_version="9" library_locally_modified="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<packageinstances>
<packageinstance name="2X03"/>
</packageinstances>
</package3d>
<package3d name="R1206" urn="urn:adsk.eagle:package:2432624/1" type="box" library_version="9" library_locally_modified="yes">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<packageinstances>
<packageinstance name="R1206"/>
</packageinstances>
</package3d>
<package3d name="R1206W" urn="urn:adsk.eagle:package:2432625/1" type="box" library_version="9" library_locally_modified="yes">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<packageinstances>
<packageinstance name="R1206W"/>
</packageinstances>
</package3d>
<package3d name="R1206FAB" urn="urn:adsk.eagle:package:2432628/2" type="model" library_version="9" library_locally_modified="yes">
<packageinstances>
<packageinstance name="R1206FAB"/>
</packageinstances>
</package3d>
<package3d name="CSRN2010" urn="urn:adsk.eagle:package:2627071/2" type="box" library_version="9" library_locally_modified="yes">
<description>&lt;p&gt;Resistence: 0.250 Ohm

&lt;p&gt;Technical specifications:
&lt;ul&gt;
&lt;li&gt;Power rating @ 70°C: 1 W
&lt;li&gt;Dielectric withstanding voltage: 200 V
&lt;/ul&gt;

&lt;p&gt;Manufacturer: Stackpole Electronics Inc.
&lt;p&gt;Digikey code: CSRN2010FKR250CT-ND
&lt;p&gt;&lt;a href="https://www.seielect.com/Catalog/SEI-CSR_CSRN.pdf"&gt;Datasheet&lt;/a&gt;</description>
<packageinstances>
<packageinstance name="CSRN2010"/>
</packageinstances>
</package3d>
<package3d name="1X06SMD" urn="urn:adsk.eagle:package:2432630/1" type="box" library_version="9" library_locally_modified="yes">
<packageinstances>
<packageinstance name="1X06SMD"/>
</packageinstances>
</package3d>
<package3d name="LED1206" urn="urn:adsk.eagle:package:2432634/1" type="box" library_version="9" library_locally_modified="yes">
<description>LED 1206 pads (standard pattern)</description>
<packageinstances>
<packageinstance name="LED1206"/>
</packageinstances>
</package3d>
<package3d name="LED1206FAB" urn="urn:adsk.eagle:package:2432633/2" type="model" library_version="9" library_locally_modified="yes">
<description>LED1206 FAB style (smaller pads to allow trace between)</description>
<packageinstances>
<packageinstance name="LED1206FAB"/>
</packageinstances>
</package3d>
<package3d name="5MM" urn="urn:adsk.eagle:package:2432635/1" type="box" library_version="9" library_locally_modified="yes">
<description>5mm round through hole part.</description>
<packageinstances>
<packageinstance name="5MM"/>
</packageinstances>
</package3d>
<package3d name="PJ-002AH-SMT-TR" urn="urn:adsk.eagle:package:2727695/1" locally_modified="yes" type="box" library_version="9" library_locally_modified="yes">
<description>&lt;p&gt;Technical specifications:
&lt;ul&gt;
&lt;li&gt;Rated Input Voltage (typ): 24 V
&lt;li&gt;Rated Input Current (max): 5 A
&lt;li&gt;Operating Temperature: -25 to 85 °C
&lt;/ul&gt;
&lt;p&gt;Digikey code: CP-002AHPJCT-ND
&lt;p&gt;
&lt;a href="https://www.cui.com/product/resource/pj-002ah-smt-tr.pdf"&gt;Datasheet&lt;/a&gt;&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="PJ-002AH-SMT-TR"/>
</packageinstances>
</package3d>
<package3d name="PJ1-023-SMT-TR" urn="urn:adsk.eagle:package:2727696/1" type="box" library_version="9" library_locally_modified="yes">
<description>&lt;p&gt;Technical specifications:
&lt;ul&gt;
&lt;li&gt;Rated Input Voltage (typ): 24 V
&lt;li&gt;Rated Input Current (max): 2.5 A
&lt;li&gt;Operating Temperature: -25 to 85 °C
&lt;/ul&gt;
&lt;p&gt;Digikey code: CP1-023PJCT-ND
&lt;p&gt;
&lt;a href="https://www.cui.com/product/resource/pj1-023-smt-tr.pdf"&gt;Datasheet&lt;/a&gt;&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="PJ1-023-SMT-TR"/>
</packageinstances>
</package3d>
<package3d name="SOIC8_PAD" urn="urn:adsk.eagle:package:2627031/1" type="box" library_version="9" library_locally_modified="yes">
<description>&lt;B&gt;Wide Plastic Gull Wing Small Outline Package&lt;/B&gt;</description>
<packageinstances>
<packageinstance name="SOIC8_PAD"/>
</packageinstances>
</package3d>
<package3d name="1X04SMD" urn="urn:adsk.eagle:package:2593472/1" type="box" library_version="9" library_locally_modified="yes">
<packageinstances>
<packageinstance name="1X04SMD"/>
</packageinstances>
</package3d>
<package3d name="QS" urn="urn:adsk.eagle:package:26647/2" locally_modified="yes" type="model" library_version="9" library_locally_modified="yes">
<description>CRYSTAL</description>
<packageinstances>
<packageinstance name="CRYSTAL_PTH"/>
</packageinstances>
</package3d>
<package3d name="B3SN-3112P" urn="urn:adsk.eagle:package:2727697/1" locally_modified="yes" type="box" library_version="9" library_locally_modified="yes">
<description>&lt;p&gt;Digikey code: SW262CT-ND

&lt;p&gt;&lt;a href="http://omronfs.omron.com/en_US/ecb/products/pdf/en-b3sn.pdf"&gt;Datasheet&lt;/a&gt;</description>
<packageinstances>
<packageinstance name="B3SN-3112P"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="DC_POWER_JACK" urn="urn:adsk.eagle:symbol:2727688/1" locally_modified="yes" library_version="9" library_locally_modified="yes">
<description>DC power jack</description>
<wire x1="-10.668" y1="3.556" x2="-5.08" y2="3.556" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="4.826" x2="-5.08" y2="8.382" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="8.382" x2="-6.858" y2="8.382" width="0.1524" layer="94"/>
<wire x1="-6.858" y1="8.382" x2="-6.858" y2="7.874" width="0.1524" layer="94"/>
<wire x1="-6.858" y1="7.874" x2="-6.858" y2="5.334" width="0.1524" layer="94"/>
<wire x1="-6.858" y1="5.334" x2="-6.858" y2="4.826" width="0.1524" layer="94"/>
<wire x1="-6.858" y1="4.826" x2="-5.08" y2="4.826" width="0.1524" layer="94"/>
<wire x1="-6.858" y1="5.334" x2="-19.812" y2="5.334" width="0.1524" layer="94"/>
<wire x1="-19.812" y1="7.874" x2="-6.858" y2="7.874" width="0.1524" layer="94"/>
<wire x1="-19.812" y1="5.334" x2="-19.812" y2="7.874" width="0.1524" layer="94" curve="-180"/>
<wire x1="-10.668" y1="3.556" x2="-10.668" y2="2.794" width="0.1524" layer="94"/>
<wire x1="-11.176" y1="2.794" x2="-10.16" y2="2.794" width="0.1524" layer="94"/>
<wire x1="-10.16" y1="2.794" x2="-10.668" y2="0" width="0.1524" layer="94"/>
<wire x1="-10.668" y1="0" x2="-11.176" y2="2.794" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="0" x2="-13.462" y2="0" width="0.1524" layer="94"/>
<wire x1="-13.462" y1="0" x2="-15.24" y2="2.286" width="0.1524" layer="94"/>
<wire x1="-15.24" y1="2.286" x2="-17.018" y2="0" width="0.1524" layer="94"/>
<pin name="PIN1" x="0" y="6.604" visible="pin" length="middle" direction="sup" rot="R180"/>
<pin name="PIN3" x="0" y="3.556" visible="pin" length="middle" direction="pas" rot="R180"/>
<pin name="PIN2" x="0" y="0" visible="pin" length="middle" direction="sup" rot="R180"/>
<text x="-20.32" y="8.382" size="1.27" layer="104">&gt;NAME</text>
</symbol>
<symbol name="1X2" urn="urn:adsk.eagle:symbol:2432537/1" library_version="9" library_locally_modified="yes">
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="-2.54" x2="5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="5.08" x2="-2.54" y2="5.08" width="0.254" layer="94"/>
<text x="-2.54" y="5.715" size="1.778" layer="95">&gt;Name</text>
<text x="0" y="4.445" size="1.4224" layer="97" ratio="9" rot="R180">1</text>
<text x="0" y="1.905" size="1.4224" layer="97" ratio="9" rot="R180">2</text>
<pin name="1" x="0" y="2.54" visible="off" length="short" direction="pas" function="dot"/>
<pin name="2" x="0" y="0" visible="off" length="short" direction="pas" function="dot"/>
</symbol>
<symbol name="SWITCH-SPDT" urn="urn:adsk.eagle:symbol:2432536/1" library_version="9" library_locally_modified="yes">
<wire x1="-2.54" y1="0" x2="2.54" y2="1.27" width="0.254" layer="94"/>
<circle x="2.54" y="2.54" radius="0.635" width="0.254" layer="94"/>
<circle x="2.54" y="-2.54" radius="0.635" width="0.254" layer="94"/>
<circle x="-2.54" y="0" radius="0.635" width="0.254" layer="94"/>
<text x="-4.064" y="4.318" size="1.778" layer="95">&gt;NAME</text>
<text x="-4.826" y="-6.096" size="1.778" layer="95">&gt;VALUE</text>
<pin name="1" x="5.08" y="2.54" visible="pad" length="short" rot="R180"/>
<pin name="2" x="-5.08" y="0" visible="pad" length="short"/>
<pin name="3" x="5.08" y="-2.54" visible="pad" length="short" rot="R180"/>
</symbol>
<symbol name="ZLDO1117" urn="urn:adsk.eagle:symbol:2727689/1" library_version="9" library_locally_modified="yes">
<wire x1="-6.35" y1="10.12" x2="-6.35" y2="7.88" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="7.88" x2="-6.35" y2="-1.27" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="-1.27" x2="0" y2="-1.27" width="0.4064" layer="94"/>
<wire x1="0" y1="-1.27" x2="8.89" y2="-1.27" width="0.4064" layer="94"/>
<wire x1="8.89" y1="-1.27" x2="8.89" y2="7.98" width="0.4064" layer="94"/>
<wire x1="8.89" y1="7.98" x2="8.89" y2="10.12" width="0.4064" layer="94"/>
<wire x1="8.89" y1="10.12" x2="-6.35" y2="10.12" width="0.4064" layer="94"/>
<wire x1="-7.62" y1="7.88" x2="-6.35" y2="7.88" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="8.89" y1="7.98" x2="10.16" y2="7.98" width="0.1524" layer="94"/>
<text x="-6.121" y="11.1776" size="1.27" layer="95">&gt;NAME</text>
<text x="0.5616" y="-3.175" size="1.27" layer="96">&gt;VALUE</text>
<pin name="VIN" x="-10.16" y="7.88" visible="pin" length="short" direction="pwr"/>
<pin name="ADJ/GND" x="0" y="-5.08" visible="pin" length="short" direction="pwr" rot="R90"/>
<pin name="VOUT" x="12.7" y="7.98" visible="pin" length="short" direction="sup" rot="R180"/>
</symbol>
<symbol name="CAP-NONPOLARIZED-1" urn="urn:adsk.eagle:symbol:2626975/1" library_version="9" library_locally_modified="yes">
<description>non-polarized capacitor</description>
<wire x1="-1.778" y1="1.524" x2="-1.778" y2="0" width="0.254" layer="94"/>
<wire x1="-1.778" y1="0" x2="-1.778" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-0.762" y1="1.524" x2="-0.762" y2="0" width="0.254" layer="94"/>
<wire x1="-0.762" y1="0" x2="-0.762" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.778" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.762" y1="0" x2="0" y2="0" width="0.1524" layer="94"/>
<text x="-3.81" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.81" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="2.54" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="ATMEGAXX8-32PIN_NOPAD" library_version="9" library_locally_modified="yes">
<wire x1="-20.32" y1="33.02" x2="17.78" y2="33.02" width="0.254" layer="94"/>
<wire x1="17.78" y1="33.02" x2="17.78" y2="-30.48" width="0.254" layer="94"/>
<wire x1="17.78" y1="-30.48" x2="-20.32" y2="-30.48" width="0.254" layer="94"/>
<wire x1="-20.32" y1="-30.48" x2="-20.32" y2="33.02" width="0.254" layer="94"/>
<text x="-20.32" y="33.782" size="1.778" layer="95">&gt;NAME</text>
<text x="-20.32" y="-33.02" size="1.778" layer="96">&gt;VALUE</text>
<pin name="PB5(SCK)" x="22.86" y="-27.94" length="middle" rot="R180"/>
<pin name="PB7(XTAL2/TOSC2)" x="-25.4" y="0" length="middle"/>
<pin name="PB6(XTAL1/TOSC1)" x="-25.4" y="5.08" length="middle"/>
<pin name="GND@3" x="-25.4" y="-22.86" length="middle"/>
<pin name="GND@5" x="-25.4" y="-25.4" length="middle"/>
<pin name="VCC@4" x="-25.4" y="22.86" length="middle"/>
<pin name="VCC@6" x="-25.4" y="20.32" length="middle"/>
<pin name="AGND" x="-25.4" y="-20.32" length="middle"/>
<pin name="AREF" x="-25.4" y="15.24" length="middle"/>
<pin name="AVCC" x="-25.4" y="25.4" length="middle"/>
<pin name="PB4(MISO)" x="22.86" y="-25.4" length="middle" rot="R180"/>
<pin name="PB3(MOSI/OC2)" x="22.86" y="-22.86" length="middle" rot="R180"/>
<pin name="PB2(SS/OC1B)" x="22.86" y="-20.32" length="middle" rot="R180"/>
<pin name="PB1(OC1A)" x="22.86" y="-17.78" length="middle" rot="R180"/>
<pin name="PB0(ICP)" x="22.86" y="-15.24" length="middle" rot="R180"/>
<pin name="PD7(AIN1)" x="22.86" y="-10.16" length="middle" rot="R180"/>
<pin name="PD6(AIN0)" x="22.86" y="-7.62" length="middle" rot="R180"/>
<pin name="PD5(T1)" x="22.86" y="-5.08" length="middle" rot="R180"/>
<pin name="PD4(XCK/T0)" x="22.86" y="-2.54" length="middle" rot="R180"/>
<pin name="PD3(INT1)" x="22.86" y="0" length="middle" rot="R180"/>
<pin name="PD2(INT0)" x="22.86" y="2.54" length="middle" rot="R180"/>
<pin name="PD1(TXD)" x="22.86" y="5.08" length="middle" rot="R180"/>
<pin name="PD0(RXD)" x="22.86" y="7.62" length="middle" rot="R180"/>
<pin name="ADC7" x="22.86" y="12.7" length="middle" rot="R180"/>
<pin name="ADC6" x="22.86" y="15.24" length="middle" rot="R180"/>
<pin name="PC5(ADC5/SCL)" x="22.86" y="17.78" length="middle" rot="R180"/>
<pin name="PC4(ADC4/SDA)" x="22.86" y="20.32" length="middle" rot="R180"/>
<pin name="PC3(ADC3)" x="22.86" y="22.86" length="middle" rot="R180"/>
<pin name="PC2(ADC2)" x="22.86" y="25.4" length="middle" rot="R180"/>
<pin name="PC1(ADC1)" x="22.86" y="27.94" length="middle" rot="R180"/>
<pin name="PC0(ADC0)" x="22.86" y="30.48" length="middle" rot="R180"/>
<pin name="PC6(/RESET)" x="-25.4" y="30.48" length="middle" function="dot"/>
</symbol>
<symbol name="AVRISP" urn="urn:adsk.eagle:symbol:2432497/1" library_version="9" library_locally_modified="yes">
<wire x1="-6.35" y1="-5.08" x2="8.89" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="8.89" y1="-5.08" x2="8.89" y2="5.08" width="0.4064" layer="94"/>
<wire x1="8.89" y1="5.08" x2="-6.35" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="5.08" x2="-6.35" y2="-5.08" width="0.4064" layer="94"/>
<text x="-6.35" y="5.715" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<text x="-5.08" y="3.175" size="1.27" layer="95">MISO</text>
<text x="-5.08" y="0.635" size="1.27" layer="95">SCK</text>
<text x="-5.08" y="-1.905" size="1.27" layer="95">RST</text>
<text x="7.62" y="3.175" size="1.27" layer="95" rot="MR0">VCC</text>
<text x="7.62" y="0.635" size="1.27" layer="95" rot="MR0">MOSI</text>
<text x="7.62" y="-1.905" size="1.27" layer="95" rot="MR0">GND</text>
<pin name="MISO" x="-2.54" y="2.54" visible="off" length="short" direction="pas" function="dot"/>
<pin name="VCC" x="5.08" y="2.54" visible="off" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="SCK" x="-2.54" y="0" visible="off" length="short" direction="pas" function="dot"/>
<pin name="MOSI" x="5.08" y="0" visible="off" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="RST" x="-2.54" y="-2.54" visible="off" length="short" direction="pas" function="dot"/>
<pin name="GND" x="5.08" y="-2.54" visible="off" length="short" direction="pas" function="dot" rot="R180"/>
</symbol>
<symbol name="R-US" urn="urn:adsk.eagle:symbol:2432514/1" library_version="9" library_locally_modified="yes">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.2032" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.2032" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.2032" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="FTDI" urn="urn:adsk.eagle:symbol:2432516/1" library_version="9" library_locally_modified="yes">
<pin name="GND" x="0" y="12.7" length="middle"/>
<pin name="CTS" x="0" y="10.16" length="middle"/>
<pin name="VCC" x="0" y="7.62" length="middle"/>
<pin name="TXD" x="0" y="5.08" length="middle"/>
<pin name="RXD" x="0" y="2.54" length="middle"/>
<pin name="RTS" x="0" y="0" length="middle"/>
<wire x1="12.7" y1="17.018" x2="2.54" y2="17.018" width="0.254" layer="94"/>
<wire x1="2.54" y1="17.018" x2="2.54" y2="-4.064" width="0.254" layer="94"/>
<wire x1="2.54" y1="-4.064" x2="12.7" y2="-4.064" width="0.254" layer="94"/>
<wire x1="12.7" y1="-4.064" x2="12.7" y2="17.018" width="0.254" layer="94"/>
<text x="3.81" y="14.478" size="1.778" layer="95">(Black)</text>
<text x="3.302" y="-3.048" size="1.778" layer="95">(Green)</text>
</symbol>
<symbol name="LED" urn="urn:adsk.eagle:symbol:2432520/1" library_version="9" library_locally_modified="yes">
<description>LED</description>
<wire x1="1.27" y1="2.54" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="2.54" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="1.778" x2="-3.429" y2="0.381" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="0.635" x2="-3.302" y2="-0.762" width="0.1524" layer="94"/>
<text x="3.556" y="-2.032" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="5.715" y="-2.032" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="C" x="0" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="0.381"/>
<vertex x="-3.048" y="1.27"/>
<vertex x="-2.54" y="0.762"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-0.762"/>
<vertex x="-2.921" y="0.127"/>
<vertex x="-2.413" y="-0.381"/>
</polygon>
</symbol>
<symbol name="PINH2X3" urn="urn:adsk.eagle:symbol:2432498/1" library_version="9" library_locally_modified="yes">
<wire x1="-6.35" y1="-5.08" x2="8.89" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="8.89" y1="-5.08" x2="8.89" y2="5.08" width="0.4064" layer="94"/>
<wire x1="8.89" y1="5.08" x2="-6.35" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="5.08" x2="-6.35" y2="-5.08" width="0.4064" layer="94"/>
<text x="-6.35" y="5.715" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="5.08" y="2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="3" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="4" x="5.08" y="0" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="5" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="6" x="5.08" y="-2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
</symbol>
<symbol name="A4953-H-BRIDGE-MOTOR-DRIVER-1" urn="urn:adsk.eagle:symbol:2626967/1" library_version="9" library_locally_modified="yes">
<wire x1="-7.62" y1="7.62" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="10.16" y1="-5.08" x2="10.16" y2="7.62" width="0.254" layer="94"/>
<wire x1="10.16" y1="7.62" x2="-7.62" y2="7.62" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="10.16" y2="-5.08" width="0.254" layer="94"/>
<pin name="GND" x="-12.7" y="5.08" length="middle"/>
<pin name="IN2" x="-12.7" y="2.54" length="middle"/>
<pin name="IN1" x="-12.7" y="0" length="middle"/>
<pin name="VREF" x="-12.7" y="-2.54" length="middle"/>
<pin name="VBB" x="15.24" y="-2.54" length="middle" rot="R180"/>
<pin name="OUT1" x="15.24" y="0" length="middle" rot="R180"/>
<pin name="LSS" x="15.24" y="2.54" length="middle" rot="R180"/>
<pin name="OUT2" x="15.24" y="5.08" length="middle" rot="R180"/>
</symbol>
<symbol name="PINH1X4" urn="urn:adsk.eagle:symbol:2593468/2" library_version="9" library_locally_modified="yes">
<description>Male SMD pin header 1x4</description>
<pin name="1" x="-5.08" y="0" visible="pin" length="middle" direction="pas"/>
<pin name="2" x="-5.08" y="-2.54" visible="pin" length="middle" direction="pas"/>
<pin name="3" x="-5.08" y="-5.08" visible="pin" length="middle" direction="pas"/>
<pin name="4" x="-5.08" y="-7.62" visible="pin" length="middle" direction="pas"/>
<wire x1="7.62" y1="1.778" x2="-2.54" y2="1.778" width="0.254" layer="94"/>
<wire x1="-2.54" y1="1.778" x2="-2.54" y2="-9.144" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-9.144" x2="7.62" y2="-9.144" width="0.254" layer="94"/>
<wire x1="7.62" y1="-9.144" x2="7.62" y2="1.778" width="0.254" layer="94"/>
<text x="-2.286" y="2.54" size="1.778" layer="95">&gt;NAME</text>
</symbol>
<symbol name="CRYSTAL_PTH" library_version="9" library_locally_modified="yes">
<wire x1="-1.27" y1="2.54" x2="1.397" y2="2.54" width="0.4064" layer="94"/>
<wire x1="1.397" y1="2.54" x2="1.397" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.397" y1="-2.54" x2="-1.27" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="-1.27" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="2.3368" y1="2.54" x2="2.3368" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="-2.286" y1="2.54" x2="-2.286" y2="-2.54" width="0.4064" layer="94"/>
<text x="-5.08" y="3.81" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="TACTILE_SWITCH_W/GND" library_version="9" library_locally_modified="yes">
<text x="-3.11" y="5.58" size="1.778" layer="95">&gt;NAME</text>
<wire x1="0.1" y1="3.5" x2="0.1" y2="4.5" width="0.254" layer="94"/>
<wire x1="0.1" y1="4.5" x2="2" y2="4.5" width="0.254" layer="94"/>
<wire x1="2" y1="4.5" x2="3.9" y2="4.5" width="0.254" layer="94"/>
<wire x1="3.9" y1="4.5" x2="3.9" y2="3.5" width="0.254" layer="94"/>
<wire x1="2" y1="4.5" x2="2" y2="3.2" width="0.254" layer="94"/>
<wire x1="2" y1="2.8" x2="2" y2="2" width="0.254" layer="94"/>
<wire x1="2" y1="1.6" x2="2" y2="0.9" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="3.7" y2="1.6" width="0.254" layer="94"/>
<wire x1="4" y1="0" x2="3.5" y2="0" width="0.254" layer="94"/>
<pin name="PIN1-2" x="-5.1" y="0" visible="pad" length="middle" direction="pas"/>
<pin name="PIN3-4" x="9.1" y="0" visible="pad" length="middle" direction="pas" rot="R180"/>
<circle x="0" y="0" radius="0.15" width="0.3" layer="94"/>
<circle x="4" y="0" radius="0.15" width="0.3" layer="94"/>
<wire x1="0.762" y1="-1.016" x2="3.048" y2="-1.016" width="0.254" layer="94"/>
<pin name="GND" x="1.905" y="-3.556" visible="pad" length="short" direction="pas" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="JACK_POWER" urn="urn:adsk.eagle:component:2727701/1" locally_modified="yes" prefix="J" library_version="9" library_locally_modified="yes">
<description>&lt;b&gt;DC Power Jack&lt;/b&gt;

&lt;p&gt;Manufacturer: CUI Inc.</description>
<gates>
<gate name="G$1" symbol="DC_POWER_JACK" x="0" y="0"/>
</gates>
<devices>
<device name="_2X5.5MM" package="PJ-002AH-SMT-TR">
<connects>
<connect gate="G$1" pin="PIN1" pad="1@1 1@2" route="any"/>
<connect gate="G$1" pin="PIN2" pad="2"/>
<connect gate="G$1" pin="PIN3" pad="3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:2727695/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_0.7X2.35MM" package="PJ1-023-SMT-TR">
<connects>
<connect gate="G$1" pin="PIN1" pad="PIN1"/>
<connect gate="G$1" pin="PIN2" pad="PIN2"/>
<connect gate="G$1" pin="PIN3" pad="PIN3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:2727696/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TERMINAL_BLOCK_1X02" urn="urn:adsk.eagle:component:2432703/4" prefix="J" library_version="9" library_locally_modified="yes">
<description>3.5mm terminal block, 2 positions ED555-2DS as found in the fablab inventory.
Holes for drilling can be exported as image. 
&lt;p&gt;Stolen from &lt;a href="http://www.ladyada.net/library/pcb/eaglelibrary.html"&gt;Adafruit&lt;/a&gt;, adapted by Zaerc.</description>
<gates>
<gate name="G$1" symbol="1X2" x="0" y="0"/>
</gates>
<devices>
<device name="-ADAFRUIT" package="3.5MMTERM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:2432654/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-FABLAB" package="ED555DS-2DS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:2432655/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-FABLAB-WITH-DRILL-HOLES" package="ED555DS-2DS-DRILL">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:2593473/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SWITCH_SLIDE" urn="urn:adsk.eagle:component:2432702/2" locally_modified="yes" prefix="S" library_version="9" library_locally_modified="yes">
<description>SMD slide-switch AYZ0102AGRLC as found in the fablab inventory.  Includes the mounting holes.

&lt;p&gt;Made by Zaerc.</description>
<gates>
<gate name="G$1" symbol="SWITCH-SPDT" x="0" y="0"/>
</gates>
<devices>
<device name="" package="AYZ0102AGRLC">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:2432653/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_DRILL_HOLES" package="AYZ0102AGRLC_W/_DRILL_HOLES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ZLDO1117" urn="urn:adsk.eagle:component:2727702/1" prefix="J" library_version="9" library_locally_modified="yes">
<gates>
<gate name="G$1" symbol="ZLDO1117" x="0" y="0"/>
</gates>
<devices>
<device name="_SOT223" package="ZLDO1117">
<connects>
<connect gate="G$1" pin="ADJ/GND" pad="GND"/>
<connect gate="G$1" pin="VIN" pad="VIN"/>
<connect gate="G$1" pin="VOUT" pad="VOUT@1 VOUT@2" route="any"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:2727698/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAP-UNPOLARIZED" urn="urn:adsk.eagle:component:2627080/1" prefix="C" uservalue="yes" library_version="9" library_locally_modified="yes">
<gates>
<gate name="&gt;NAME" symbol="CAP-NONPOLARIZED-1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="C1206">
<connects>
<connect gate="&gt;NAME" pin="1" pad="1"/>
<connect gate="&gt;NAME" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:2432626/3"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="FAB" package="C1206FAB">
<connects>
<connect gate="&gt;NAME" pin="1" pad="1"/>
<connect gate="&gt;NAME" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:2432629/3"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2220" package="C2220">
<connects>
<connect gate="&gt;NAME" pin="1" pad="1"/>
<connect gate="&gt;NAME" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:2627042/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ATMEGA328P_TQFP" prefix="U" library_version="9" library_locally_modified="yes">
<description>&lt;h3&gt;Popular 328P in QFP&lt;/h3&gt;
&lt;p&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href="https://www.sparkfun.com/products/13448"&gt;Storefront component: ATmega328 - TQFP&lt;/a&gt; (COM-13448)&lt;/p&gt;
&lt;p&gt;&lt;a href="http://www.sparkfun.com/datasheets/Components/SMD/ATMega328.pdf"&gt;Datasheet&lt;/a&gt;&lt;/p&gt;
&lt;h4&gt;SparkFun Products&lt;/h4&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/12757"&gt;SparkFun RedBoard - Programmed with Arduino&lt;/a&gt; (DEV-12757)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11113"&gt;Arduino Pro Mini 328 - 5V/16MHz&lt;/a&gt; (DEV-11113)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11114"&gt;Arduino Pro Mini 328 - 3.3V/8MHz&lt;/a&gt; (DEV-11114)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/13712"&gt;SparkFun OpenLog&lt;/a&gt; (DEV-13712)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/12923"&gt;SparkFun MicroView - OLED Arduino Module&lt;/a&gt; (DEV-12923)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/12651"&gt;SparkFun Digital Sandbox&lt;/a&gt; (DEV-12651)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11888"&gt;SparkFun PicoBoard&lt;/a&gt; (WIG-11888)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/10116"&gt;Arduino Fio&lt;/a&gt; (DEV-10116)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/13342"&gt;LilyPad Arduino 328 Main Board&lt;/a&gt; (DEV-13342)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/13741"&gt;SparkFun RedStick&lt;/a&gt; (DEV-13741)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11013"&gt;LilyPad MP3&lt;/a&gt; (DEV-11013)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/10941"&gt;LilyPad Arduino SimpleSnap&lt;/a&gt; (DEV-10941)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/13899"&gt;SparkFun Stepoko&lt;/a&gt; (ROB-13899)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11345"&gt;SparkFun Geiger Counter&lt;/a&gt; (SEN-11345)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/12781"&gt;SparkFun EL Sequencer&lt;/a&gt; (COM-12781)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/10915"&gt;Arduino Pro 328 - 5V/16MHz&lt;/a&gt; (DEV-10915)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/12773"&gt;CryptoCape&lt;/a&gt; (DEV-12773)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/10740"&gt;SparkFun IR Thermometer Evaluation Board - MLX90614&lt;/a&gt; (SEN-10740)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/12097"&gt;SparkFun RedBot Mainboard&lt;/a&gt; (ROB-12097)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/10914"&gt;Arduino Pro 328 - 3.3V/8MHz&lt;/a&gt; (DEV-10914)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/13036"&gt;SparkFun Block for Intel® Edison - Arduino&lt;/a&gt; (DEV-13036)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/10935"&gt;SparkFun Simon Says - Surface Mount Soldering Kit&lt;/a&gt; (KIT-10935)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11441"&gt;SparkFun 7-Segment Serial Display - Red&lt;/a&gt; (COM-11441)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11629"&gt;SparkFun 7-Segment Serial Display - White&lt;/a&gt; (COM-11629)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11443"&gt;SparkFun 7-Segment Serial Display - Yellow&lt;/a&gt; (COM-11443)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11440"&gt;SparkFun 7-Segment Serial Display - Kelly Green&lt;/a&gt; (COM-11440)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11442"&gt;SparkFun 7-Segment Serial Display - Blue&lt;/a&gt; (COM-11442)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11647"&gt;SparkFun OpenSegment Serial Display - 20mm (Blue)&lt;/a&gt; (COM-11647)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11646"&gt;SparkFun OpenSegment Serial Display - 20mm (Green)&lt;/a&gt; (COM-11646)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11648"&gt;SparkFun OpenSegment Serial Display - 20mm (White)&lt;/a&gt; (COM-11648)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11644"&gt;SparkFun OpenSegment Serial Display - 20mm (Red)&lt;/a&gt; (COM-11644)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11645"&gt;SparkFun OpenSegment Serial Display - 20mm (Yellow)&lt;/a&gt; (COM-11645)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/13190"&gt;SparkFun OpenSegment Shield&lt;/a&gt; (DEV-13190)&lt;/li&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="U$1" symbol="ATMEGAXX8-32PIN_NOPAD" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TQFP32-08THIN">
<connects>
<connect gate="U$1" pin="ADC6" pad="19"/>
<connect gate="U$1" pin="ADC7" pad="22"/>
<connect gate="U$1" pin="AGND" pad="21"/>
<connect gate="U$1" pin="AREF" pad="20"/>
<connect gate="U$1" pin="AVCC" pad="18"/>
<connect gate="U$1" pin="GND@3" pad="3"/>
<connect gate="U$1" pin="GND@5" pad="5"/>
<connect gate="U$1" pin="PB0(ICP)" pad="12"/>
<connect gate="U$1" pin="PB1(OC1A)" pad="13"/>
<connect gate="U$1" pin="PB2(SS/OC1B)" pad="14"/>
<connect gate="U$1" pin="PB3(MOSI/OC2)" pad="15"/>
<connect gate="U$1" pin="PB4(MISO)" pad="16"/>
<connect gate="U$1" pin="PB5(SCK)" pad="17"/>
<connect gate="U$1" pin="PB6(XTAL1/TOSC1)" pad="7"/>
<connect gate="U$1" pin="PB7(XTAL2/TOSC2)" pad="8"/>
<connect gate="U$1" pin="PC0(ADC0)" pad="23"/>
<connect gate="U$1" pin="PC1(ADC1)" pad="24"/>
<connect gate="U$1" pin="PC2(ADC2)" pad="25"/>
<connect gate="U$1" pin="PC3(ADC3)" pad="26"/>
<connect gate="U$1" pin="PC4(ADC4/SDA)" pad="27"/>
<connect gate="U$1" pin="PC5(ADC5/SCL)" pad="28"/>
<connect gate="U$1" pin="PC6(/RESET)" pad="29"/>
<connect gate="U$1" pin="PD0(RXD)" pad="30"/>
<connect gate="U$1" pin="PD1(TXD)" pad="31"/>
<connect gate="U$1" pin="PD2(INT0)" pad="32"/>
<connect gate="U$1" pin="PD3(INT1)" pad="1"/>
<connect gate="U$1" pin="PD4(XCK/T0)" pad="2"/>
<connect gate="U$1" pin="PD5(T1)" pad="9"/>
<connect gate="U$1" pin="PD6(AIN0)" pad="10"/>
<connect gate="U$1" pin="PD7(AIN1)" pad="11"/>
<connect gate="U$1" pin="VCC@4" pad="4"/>
<connect gate="U$1" pin="VCC@6" pad="6"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:2432619/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="AVRISP" urn="urn:adsk.eagle:component:2432662/3" library_version="9" library_locally_modified="yes">
<gates>
<gate name="G$1" symbol="AVRISP" x="0" y="0"/>
</gates>
<devices>
<device name="SMD" package="2X03SMD">
<connects>
<connect gate="G$1" pin="GND" pad="6"/>
<connect gate="G$1" pin="MISO" pad="1"/>
<connect gate="G$1" pin="MOSI" pad="4"/>
<connect gate="G$1" pin="RST" pad="5"/>
<connect gate="G$1" pin="SCK" pad="3"/>
<connect gate="G$1" pin="VCC" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:2432609/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="2X03">
<connects>
<connect gate="G$1" pin="GND" pad="6"/>
<connect gate="G$1" pin="MISO" pad="1"/>
<connect gate="G$1" pin="MOSI" pad="4"/>
<connect gate="G$1" pin="RST" pad="5"/>
<connect gate="G$1" pin="SCK" pad="3"/>
<connect gate="G$1" pin="VCC" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:2432603/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RESISTOR" urn="urn:adsk.eagle:component:2432681/5" prefix="R" uservalue="yes" library_version="9" library_locally_modified="yes">
<description>&lt;b&gt;Resistor (US Symbol)&lt;/b&gt;
&lt;p&gt;
Variants with postfix FAB are widened to allow the routing of internal traces</description>
<gates>
<gate name="G$1" symbol="R-US" x="0" y="0"/>
</gates>
<devices>
<device name="1206" package="R1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:2432624/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206W" package="R1206W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:2432625/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206FAB" package="R1206FAB">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:2432628/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_CSRN2010" package="CSRN2010">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:2627071/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FTDI-SMD-HEADER" urn="urn:adsk.eagle:component:2432682/1" library_version="9" library_locally_modified="yes">
<gates>
<gate name="G$1" symbol="FTDI" x="-7.62" y="-5.08"/>
</gates>
<devices>
<device name="" package="1X06SMD">
<connects>
<connect gate="G$1" pin="CTS" pad="2"/>
<connect gate="G$1" pin="GND" pad="1"/>
<connect gate="G$1" pin="RTS" pad="6"/>
<connect gate="G$1" pin="RXD" pad="5"/>
<connect gate="G$1" pin="TXD" pad="4"/>
<connect gate="G$1" pin="VCC" pad="3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:2432630/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LED" urn="urn:adsk.eagle:component:2432686/3" library_version="9" library_locally_modified="yes">
<description>LED</description>
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LED1206">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:2432634/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="FAB1206" package="LED1206FAB">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:2432633/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5MM" package="5MM">
<connects>
<connect gate="G$1" pin="A" pad="IN"/>
<connect gate="G$1" pin="C" pad="OUT"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:2432635/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PIN_HEADER_SMD_2X3" urn="urn:adsk.eagle:component:2432663/4" prefix="JP" uservalue="yes" library_version="9" library_locally_modified="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="PINH2X3" x="0" y="0"/>
</gates>
<devices>
<device name="" package="2X03SMD">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:2432609/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="A4953-H-BRIDGE-MOTOR-DRIVER-PAD" urn="urn:adsk.eagle:component:2627072/1" library_version="9" library_locally_modified="yes">
<gates>
<gate name="G$1" symbol="A4953-H-BRIDGE-MOTOR-DRIVER-1" x="15.24" y="-5.08"/>
</gates>
<devices>
<device name="" package="SOIC8_PAD">
<connects>
<connect gate="G$1" pin="GND" pad="1 GND"/>
<connect gate="G$1" pin="IN1" pad="3"/>
<connect gate="G$1" pin="IN2" pad="2"/>
<connect gate="G$1" pin="LSS" pad="7"/>
<connect gate="G$1" pin="OUT1" pad="6"/>
<connect gate="G$1" pin="OUT2" pad="8"/>
<connect gate="G$1" pin="VBB" pad="5"/>
<connect gate="G$1" pin="VREF" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:2627031/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PIN_HEADER_SMD_1X4" urn="urn:adsk.eagle:component:2593474/3" prefix="J" library_version="9" library_locally_modified="yes">
<gates>
<gate name="G$1" symbol="PINH1X4" x="5.08" y="0"/>
</gates>
<devices>
<device name="" package="1X04SMD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3" route="any"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:2593472/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CRYSTAL_PTH" prefix="Q" uservalue="yes" library_version="9" library_locally_modified="yes">
<description>&lt;B&gt;CRYSTAL&lt;/B&gt;</description>
<gates>
<gate name="G$1" symbol="CRYSTAL_PTH" x="0" y="0"/>
</gates>
<devices>
<device name="" package="CRYSTAL_PTH">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26647/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SWITCH_TACTILE_W/GND" prefix="S" uservalue="yes" library_version="9" library_locally_modified="yes">
<description>&lt;b&gt;Sealed Tactile Switch (SMT)&lt;/b&gt;

&lt;p&gt;Technical specifications:
&lt;ul&gt;
&lt;li&gt;Rated voltage: 3 to 24 VDC
&lt;li&gt;Rated current: 1 to 50 mA
&lt;li&gt;Operating temperatue: -25 to 70°C
&lt;/ul&gt;

&lt;p&gt;Manufacturer: Omron</description>
<gates>
<gate name="G$1" symbol="TACTILE_SWITCH_W/GND" x="0" y="0"/>
</gates>
<devices>
<device name="_W/_GND" package="B3SN-3112P">
<connects>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="PIN1-2" pad="1 2" route="any"/>
<connect gate="G$1" pin="PIN3-4" pad="3 4" route="any"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:2727697/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1" urn="urn:adsk.eagle:library:371">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26925/1" library_version="1">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="VCC" urn="urn:adsk.eagle:symbol:26928/1" library_version="1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="VCC" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" urn="urn:adsk.eagle:component:26954/1" prefix="GND" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VCC" urn="urn:adsk.eagle:component:26957/1" prefix="P+" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="VCC" symbol="VCC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="J1" library="fab" library_urn="urn:adsk.eagle:library:2432493" deviceset="JACK_POWER" device="_2X5.5MM" package3d_urn="urn:adsk.eagle:package:2727695/1"/>
<part name="GND1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="J2" library="fab" library_urn="urn:adsk.eagle:library:2432493" deviceset="TERMINAL_BLOCK_1X02" device="-FABLAB-WITH-DRILL-HOLES" package3d_urn="urn:adsk.eagle:package:2593473/2"/>
<part name="J3" library="fab" library_urn="urn:adsk.eagle:library:2432493" deviceset="TERMINAL_BLOCK_1X02" device="-FABLAB-WITH-DRILL-HOLES" package3d_urn="urn:adsk.eagle:package:2593473/2"/>
<part name="S1" library="fab" library_urn="urn:adsk.eagle:library:2432493" deviceset="SWITCH_SLIDE" device="_DRILL_HOLES" value="SWITCH"/>
<part name="J4" library="fab" library_urn="urn:adsk.eagle:library:2432493" deviceset="ZLDO1117" device="_SOT223" package3d_urn="urn:adsk.eagle:package:2727698/1"/>
<part name="C1" library="fab" library_urn="urn:adsk.eagle:library:2432493" deviceset="CAP-UNPOLARIZED" device="FAB" package3d_urn="urn:adsk.eagle:package:2432629/3" value="1uF"/>
<part name="C2" library="fab" library_urn="urn:adsk.eagle:library:2432493" deviceset="CAP-UNPOLARIZED" device="FAB" package3d_urn="urn:adsk.eagle:package:2432629/3" value="10uF"/>
<part name="GND2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="U1" library="fab" library_urn="urn:adsk.eagle:library:2432493" deviceset="ATMEGA328P_TQFP" device="" package3d_urn="urn:adsk.eagle:package:2432619/1"/>
<part name="GND4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C3" library="fab" library_urn="urn:adsk.eagle:library:2432493" deviceset="CAP-UNPOLARIZED" device="FAB" package3d_urn="urn:adsk.eagle:package:2432629/3" value="22pF"/>
<part name="C4" library="fab" library_urn="urn:adsk.eagle:library:2432493" deviceset="CAP-UNPOLARIZED" device="FAB" package3d_urn="urn:adsk.eagle:package:2432629/3" value="22pF"/>
<part name="C5" library="fab" library_urn="urn:adsk.eagle:library:2432493" deviceset="CAP-UNPOLARIZED" device="FAB" package3d_urn="urn:adsk.eagle:package:2432629/3" value="0.1uF"/>
<part name="C6" library="fab" library_urn="urn:adsk.eagle:library:2432493" deviceset="CAP-UNPOLARIZED" device="FAB" package3d_urn="urn:adsk.eagle:package:2432629/3" value="0.1uF"/>
<part name="GND5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND6" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="P+2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="P+1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="U$1" library="fab" library_urn="urn:adsk.eagle:library:2432493" deviceset="AVRISP" device="SMD" package3d_urn="urn:adsk.eagle:package:2432609/2"/>
<part name="GND7" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="P+3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="C7" library="fab" library_urn="urn:adsk.eagle:library:2432493" deviceset="CAP-UNPOLARIZED" device="FAB" package3d_urn="urn:adsk.eagle:package:2432629/3" value="0.1uF"/>
<part name="GND8" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="P+4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="R1" library="fab" library_urn="urn:adsk.eagle:library:2432493" deviceset="RESISTOR" device="1206FAB" package3d_urn="urn:adsk.eagle:package:2432628/2" value="10kOhm"/>
<part name="U$2" library="fab" library_urn="urn:adsk.eagle:library:2432493" deviceset="FTDI-SMD-HEADER" device="" package3d_urn="urn:adsk.eagle:package:2432630/1"/>
<part name="GND9" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="S3" library="fab" library_urn="urn:adsk.eagle:library:2432493" deviceset="SWITCH_SLIDE" device="_DRILL_HOLES" value="SWITCH"/>
<part name="U$3" library="fab" library_urn="urn:adsk.eagle:library:2432493" deviceset="LED" device="FAB1206" package3d_urn="urn:adsk.eagle:package:2432633/2"/>
<part name="U$4" library="fab" library_urn="urn:adsk.eagle:library:2432493" deviceset="LED" device="FAB1206" package3d_urn="urn:adsk.eagle:package:2432633/2"/>
<part name="U$5" library="fab" library_urn="urn:adsk.eagle:library:2432493" deviceset="LED" device="FAB1206" package3d_urn="urn:adsk.eagle:package:2432633/2"/>
<part name="R2" library="fab" library_urn="urn:adsk.eagle:library:2432493" deviceset="RESISTOR" device="1206FAB" package3d_urn="urn:adsk.eagle:package:2432628/2" value="1kOhm"/>
<part name="R3" library="fab" library_urn="urn:adsk.eagle:library:2432493" deviceset="RESISTOR" device="1206FAB" package3d_urn="urn:adsk.eagle:package:2432628/2" value="499Ohm"/>
<part name="R4" library="fab" library_urn="urn:adsk.eagle:library:2432493" deviceset="RESISTOR" device="1206FAB" package3d_urn="urn:adsk.eagle:package:2432628/2" value="499Ohm"/>
<part name="GND10" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="P+5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="JP1" library="fab" library_urn="urn:adsk.eagle:library:2432493" deviceset="PIN_HEADER_SMD_2X3" device="" package3d_urn="urn:adsk.eagle:package:2432609/2"/>
<part name="P+9" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="GND11" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND12" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="P+8" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="U$6" library="fab" library_urn="urn:adsk.eagle:library:2432493" deviceset="A4953-H-BRIDGE-MOTOR-DRIVER-PAD" device="" package3d_urn="urn:adsk.eagle:package:2627031/1"/>
<part name="GND13" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="P+7" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="GND14" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C8" library="fab" library_urn="urn:adsk.eagle:library:2432493" deviceset="CAP-UNPOLARIZED" device="FAB" package3d_urn="urn:adsk.eagle:package:2432629/3" value="0.1uF"/>
<part name="C9" library="fab" library_urn="urn:adsk.eagle:library:2432493" deviceset="CAP-UNPOLARIZED" device="FAB" package3d_urn="urn:adsk.eagle:package:2432629/3" value="10uF"/>
<part name="GND15" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="J5" library="fab" library_urn="urn:adsk.eagle:library:2432493" deviceset="TERMINAL_BLOCK_1X02" device="-FABLAB-WITH-DRILL-HOLES" package3d_urn="urn:adsk.eagle:package:2593473/2"/>
<part name="J6" library="fab" library_urn="urn:adsk.eagle:library:2432493" deviceset="PIN_HEADER_SMD_1X4" device="" package3d_urn="urn:adsk.eagle:package:2593472/1"/>
<part name="P+10" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="GND16" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="Q1" library="fab" library_urn="urn:adsk.eagle:library:2432493" deviceset="CRYSTAL_PTH" device="" package3d_urn="urn:adsk.eagle:package:26647/2" value="16MHz"/>
<part name="S2" library="fab" library_urn="urn:adsk.eagle:library:2432493" deviceset="SWITCH_TACTILE_W/GND" device="_W/_GND" package3d_urn="urn:adsk.eagle:package:2727697/1"/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="J1" gate="G$1" x="22.86" y="111.76"/>
<instance part="GND1" gate="1" x="27.94" y="106.68"/>
<instance part="J2" gate="G$1" x="12.7" y="101.6" rot="R180"/>
<instance part="J3" gate="G$1" x="50.8" y="114.3" rot="R90"/>
<instance part="S1" gate="G$1" x="40.64" y="101.6" rot="R180"/>
<instance part="J4" gate="G$1" x="73.66" y="101.6"/>
<instance part="C1" gate="&gt;NAME" x="60.96" y="104.14" rot="R90"/>
<instance part="C2" gate="&gt;NAME" x="91.44" y="104.14" rot="R90"/>
<instance part="GND2" gate="1" x="17.78" y="93.98"/>
<instance part="GND3" gate="1" x="73.66" y="91.44"/>
<instance part="U1" gate="U$1" x="91.44" y="35.56"/>
<instance part="GND4" gate="1" x="63.5" y="5.08"/>
<instance part="C3" gate="&gt;NAME" x="48.26" y="27.94" rot="R90"/>
<instance part="C4" gate="&gt;NAME" x="63.5" y="27.94" rot="R90"/>
<instance part="C5" gate="&gt;NAME" x="60.96" y="50.8"/>
<instance part="C6" gate="&gt;NAME" x="53.34" y="58.42" rot="R90"/>
<instance part="GND5" gate="1" x="55.88" y="17.78"/>
<instance part="GND6" gate="1" x="53.34" y="45.72"/>
<instance part="P+2" gate="VCC" x="53.34" y="66.04"/>
<instance part="P+1" gate="VCC" x="124.46" y="116.84"/>
<instance part="U$1" gate="G$1" x="12.7" y="15.24"/>
<instance part="GND7" gate="1" x="25.4" y="7.62"/>
<instance part="P+3" gate="VCC" x="25.4" y="25.4"/>
<instance part="C7" gate="&gt;NAME" x="15.24" y="53.34"/>
<instance part="GND8" gate="1" x="20.32" y="33.02"/>
<instance part="P+4" gate="VCC" x="20.32" y="71.12"/>
<instance part="R1" gate="G$1" x="20.32" y="60.96" rot="R90"/>
<instance part="U$2" gate="G$1" x="160.02" y="53.34"/>
<instance part="GND9" gate="1" x="167.64" y="73.66"/>
<instance part="S3" gate="G$1" x="111.76" y="111.76" rot="R180"/>
<instance part="U$3" gate="G$1" x="157.48" y="111.76"/>
<instance part="U$4" gate="G$1" x="170.18" y="101.6"/>
<instance part="U$5" gate="G$1" x="182.88" y="101.6"/>
<instance part="R2" gate="G$1" x="157.48" y="101.6" rot="R90"/>
<instance part="R3" gate="G$1" x="170.18" y="116.84" rot="R90"/>
<instance part="R4" gate="G$1" x="182.88" y="116.84" rot="R90"/>
<instance part="GND10" gate="1" x="170.18" y="91.44"/>
<instance part="P+5" gate="VCC" x="157.48" y="132.08"/>
<instance part="JP1" gate="A" x="165.1" y="35.56"/>
<instance part="P+9" gate="VCC" x="144.78" y="43.18"/>
<instance part="GND11" gate="1" x="144.78" y="30.48"/>
<instance part="GND12" gate="1" x="187.96" y="30.48"/>
<instance part="P+8" gate="VCC" x="187.96" y="43.18"/>
<instance part="U$6" gate="G$1" x="231.14" y="48.26"/>
<instance part="GND13" gate="1" x="208.28" y="55.88"/>
<instance part="P+7" gate="VCC" x="213.36" y="43.18"/>
<instance part="GND14" gate="1" x="261.62" y="45.72"/>
<instance part="C8" gate="&gt;NAME" x="248.92" y="35.56" rot="R90"/>
<instance part="C9" gate="&gt;NAME" x="259.08" y="35.56" rot="R90"/>
<instance part="GND15" gate="1" x="248.92" y="25.4"/>
<instance part="J5" gate="G$1" x="233.68" y="20.32"/>
<instance part="J6" gate="G$1" x="172.72" y="15.24"/>
<instance part="P+10" gate="VCC" x="152.4" y="20.32"/>
<instance part="GND16" gate="1" x="152.4" y="7.62"/>
<instance part="Q1" gate="G$1" x="55.88" y="33.02"/>
<instance part="S2" gate="G$1" x="20.32" y="43.18" rot="R90"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="PIN2"/>
<wire x1="22.86" y1="111.76" x2="27.94" y2="111.76" width="0.1524" layer="91"/>
<pinref part="GND1" gate="1" pin="GND"/>
<wire x1="27.94" y1="111.76" x2="27.94" y2="109.22" width="0.1524" layer="91"/>
<pinref part="J1" gate="G$1" pin="PIN3"/>
<wire x1="22.86" y1="115.316" x2="27.94" y2="115.316" width="0.1524" layer="91"/>
<wire x1="27.94" y1="115.316" x2="27.94" y2="111.76" width="0.1524" layer="91"/>
<junction x="27.94" y="111.76"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="1"/>
<wire x1="12.7" y1="99.06" x2="17.78" y2="99.06" width="0.1524" layer="91"/>
<pinref part="GND2" gate="1" pin="GND"/>
<wire x1="17.78" y1="99.06" x2="17.78" y2="96.52" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND3" gate="1" pin="GND"/>
<pinref part="C1" gate="&gt;NAME" pin="1"/>
<wire x1="60.96" y1="99.06" x2="60.96" y2="93.98" width="0.1524" layer="91"/>
<wire x1="60.96" y1="93.98" x2="73.66" y2="93.98" width="0.1524" layer="91"/>
<junction x="73.66" y="93.98"/>
<pinref part="C2" gate="&gt;NAME" pin="1"/>
<wire x1="91.44" y1="99.06" x2="91.44" y2="93.98" width="0.1524" layer="91"/>
<wire x1="91.44" y1="93.98" x2="73.66" y2="93.98" width="0.1524" layer="91"/>
<pinref part="J4" gate="G$1" pin="ADJ/GND"/>
<wire x1="73.66" y1="96.52" x2="73.66" y2="93.98" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND4" gate="1" pin="GND"/>
<pinref part="U1" gate="U$1" pin="AGND"/>
<wire x1="66.04" y1="15.24" x2="63.5" y2="15.24" width="0.1524" layer="91"/>
<wire x1="63.5" y1="15.24" x2="63.5" y2="12.7" width="0.1524" layer="91"/>
<pinref part="U1" gate="U$1" pin="GND@3"/>
<wire x1="63.5" y1="12.7" x2="63.5" y2="10.16" width="0.1524" layer="91"/>
<wire x1="63.5" y1="10.16" x2="63.5" y2="7.62" width="0.1524" layer="91"/>
<wire x1="66.04" y1="12.7" x2="63.5" y2="12.7" width="0.1524" layer="91"/>
<junction x="63.5" y="12.7"/>
<pinref part="U1" gate="U$1" pin="GND@5"/>
<wire x1="66.04" y1="10.16" x2="63.5" y2="10.16" width="0.1524" layer="91"/>
<junction x="63.5" y="10.16"/>
</segment>
<segment>
<pinref part="C3" gate="&gt;NAME" pin="1"/>
<wire x1="48.26" y1="22.86" x2="48.26" y2="20.32" width="0.1524" layer="91"/>
<pinref part="GND5" gate="1" pin="GND"/>
<wire x1="48.26" y1="20.32" x2="55.88" y2="20.32" width="0.1524" layer="91"/>
<pinref part="C4" gate="&gt;NAME" pin="1"/>
<wire x1="55.88" y1="20.32" x2="63.5" y2="20.32" width="0.1524" layer="91"/>
<wire x1="63.5" y1="20.32" x2="63.5" y2="22.86" width="0.1524" layer="91"/>
<junction x="55.88" y="20.32"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="GND"/>
<wire x1="17.78" y1="12.7" x2="25.4" y2="12.7" width="0.1524" layer="91"/>
<pinref part="GND7" gate="1" pin="GND"/>
<wire x1="25.4" y1="12.7" x2="25.4" y2="10.16" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND8" gate="1" pin="GND"/>
<wire x1="20.32" y1="35.56" x2="20.32" y2="38.08" width="0.1524" layer="91"/>
<pinref part="S2" gate="G$1" pin="PIN1-2"/>
<pinref part="S2" gate="G$1" pin="GND"/>
<wire x1="23.876" y1="45.085" x2="23.876" y2="35.56" width="0.1524" layer="91"/>
<wire x1="23.876" y1="35.56" x2="20.32" y2="35.56" width="0.1524" layer="91"/>
<junction x="20.32" y="35.56"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="CTS"/>
<wire x1="160.02" y1="63.5" x2="157.48" y2="63.5" width="0.1524" layer="91"/>
<wire x1="157.48" y1="63.5" x2="157.48" y2="66.04" width="0.1524" layer="91"/>
<pinref part="GND9" gate="1" pin="GND"/>
<wire x1="157.48" y1="66.04" x2="157.48" y2="76.2" width="0.1524" layer="91"/>
<wire x1="157.48" y1="76.2" x2="167.64" y2="76.2" width="0.1524" layer="91"/>
<pinref part="U$2" gate="G$1" pin="GND"/>
<wire x1="160.02" y1="66.04" x2="157.48" y2="66.04" width="0.1524" layer="91"/>
<junction x="157.48" y="66.04"/>
</segment>
<segment>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="157.48" y1="96.52" x2="157.48" y2="93.98" width="0.1524" layer="91"/>
<pinref part="GND10" gate="1" pin="GND"/>
<wire x1="157.48" y1="93.98" x2="170.18" y2="93.98" width="0.1524" layer="91"/>
<wire x1="182.88" y1="93.98" x2="170.18" y2="93.98" width="0.1524" layer="91"/>
<junction x="170.18" y="93.98"/>
<pinref part="U$5" gate="G$1" pin="C"/>
<wire x1="182.88" y1="99.06" x2="182.88" y2="93.98" width="0.1524" layer="91"/>
<pinref part="U$4" gate="G$1" pin="C"/>
<wire x1="170.18" y1="99.06" x2="170.18" y2="93.98" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C5" gate="&gt;NAME" pin="1"/>
<wire x1="55.88" y1="50.8" x2="53.34" y2="50.8" width="0.1524" layer="91"/>
<pinref part="GND6" gate="1" pin="GND"/>
<wire x1="53.34" y1="50.8" x2="53.34" y2="48.26" width="0.1524" layer="91"/>
<pinref part="C6" gate="&gt;NAME" pin="1"/>
<wire x1="53.34" y1="53.34" x2="53.34" y2="48.26" width="0.1524" layer="91"/>
<junction x="53.34" y="48.26"/>
</segment>
<segment>
<pinref part="JP1" gate="A" pin="3"/>
<wire x1="162.56" y1="35.56" x2="144.78" y2="35.56" width="0.1524" layer="91"/>
<pinref part="GND11" gate="1" pin="GND"/>
<wire x1="144.78" y1="35.56" x2="144.78" y2="33.02" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="JP1" gate="A" pin="4"/>
<wire x1="170.18" y1="35.56" x2="187.96" y2="35.56" width="0.1524" layer="91"/>
<pinref part="GND12" gate="1" pin="GND"/>
<wire x1="187.96" y1="35.56" x2="187.96" y2="33.02" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$6" gate="G$1" pin="IN2"/>
<wire x1="218.44" y1="50.8" x2="215.9" y2="50.8" width="0.1524" layer="91"/>
<wire x1="215.9" y1="50.8" x2="215.9" y2="53.34" width="0.1524" layer="91"/>
<pinref part="GND13" gate="1" pin="GND"/>
<wire x1="215.9" y1="53.34" x2="215.9" y2="58.42" width="0.1524" layer="91"/>
<wire x1="215.9" y1="58.42" x2="208.28" y2="58.42" width="0.1524" layer="91"/>
<pinref part="U$6" gate="G$1" pin="GND"/>
<wire x1="218.44" y1="53.34" x2="215.9" y2="53.34" width="0.1524" layer="91"/>
<junction x="215.9" y="53.34"/>
</segment>
<segment>
<pinref part="U$6" gate="G$1" pin="LSS"/>
<wire x1="246.38" y1="50.8" x2="261.62" y2="50.8" width="0.1524" layer="91"/>
<pinref part="GND14" gate="1" pin="GND"/>
<wire x1="261.62" y1="50.8" x2="261.62" y2="48.26" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C9" gate="&gt;NAME" pin="1"/>
<pinref part="C8" gate="&gt;NAME" pin="1"/>
<wire x1="259.08" y1="30.48" x2="248.92" y2="30.48" width="0.1524" layer="91"/>
<pinref part="GND15" gate="1" pin="GND"/>
<wire x1="248.92" y1="27.94" x2="248.92" y2="30.48" width="0.1524" layer="91"/>
<junction x="248.92" y="30.48"/>
</segment>
<segment>
<pinref part="J6" gate="G$1" pin="2"/>
<wire x1="167.64" y1="12.7" x2="152.4" y2="12.7" width="0.1524" layer="91"/>
<pinref part="GND16" gate="1" pin="GND"/>
<wire x1="152.4" y1="12.7" x2="152.4" y2="10.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PIN1" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="PIN1"/>
<wire x1="22.86" y1="118.364" x2="33.02" y2="118.364" width="0.1524" layer="91"/>
<pinref part="S1" gate="G$1" pin="3"/>
<wire x1="35.56" y1="104.14" x2="33.02" y2="104.14" width="0.1524" layer="91"/>
<wire x1="33.02" y1="104.14" x2="33.02" y2="118.364" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="J3" gate="G$1" pin="1"/>
<pinref part="S1" gate="G$1" pin="2"/>
<wire x1="45.72" y1="101.6" x2="48.26" y2="101.6" width="0.1524" layer="91"/>
<wire x1="48.26" y1="101.6" x2="48.26" y2="114.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="J2" gate="G$1" pin="2"/>
<wire x1="12.7" y1="101.6" x2="22.86" y2="101.6" width="0.1524" layer="91"/>
<wire x1="22.86" y1="101.6" x2="22.86" y2="99.06" width="0.1524" layer="91"/>
<pinref part="S1" gate="G$1" pin="1"/>
<wire x1="22.86" y1="99.06" x2="35.56" y2="99.06" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="C3" gate="&gt;NAME" pin="2"/>
<wire x1="48.26" y1="30.48" x2="48.26" y2="33.02" width="0.1524" layer="91"/>
<wire x1="48.26" y1="33.02" x2="50.8" y2="33.02" width="0.1524" layer="91"/>
<pinref part="U1" gate="U$1" pin="PB6(XTAL1/TOSC1)"/>
<wire x1="66.04" y1="40.64" x2="48.26" y2="40.64" width="0.1524" layer="91"/>
<wire x1="48.26" y1="40.64" x2="48.26" y2="33.02" width="0.1524" layer="91"/>
<junction x="48.26" y="33.02"/>
<pinref part="Q1" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<wire x1="60.96" y1="33.02" x2="63.5" y2="33.02" width="0.1524" layer="91"/>
<pinref part="C4" gate="&gt;NAME" pin="2"/>
<wire x1="63.5" y1="33.02" x2="63.5" y2="30.48" width="0.1524" layer="91"/>
<pinref part="U1" gate="U$1" pin="PB7(XTAL2/TOSC2)"/>
<wire x1="66.04" y1="35.56" x2="63.5" y2="35.56" width="0.1524" layer="91"/>
<wire x1="63.5" y1="35.56" x2="63.5" y2="33.02" width="0.1524" layer="91"/>
<junction x="63.5" y="33.02"/>
<pinref part="Q1" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="U1" gate="U$1" pin="AREF"/>
<pinref part="C5" gate="&gt;NAME" pin="2"/>
<wire x1="66.04" y1="50.8" x2="63.5" y2="50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VCC" class="0">
<segment>
<pinref part="P+2" gate="VCC" pin="VCC"/>
<pinref part="C6" gate="&gt;NAME" pin="2"/>
<wire x1="53.34" y1="63.5" x2="53.34" y2="60.96" width="0.1524" layer="91"/>
<pinref part="U1" gate="U$1" pin="AVCC"/>
<wire x1="66.04" y1="60.96" x2="63.5" y2="60.96" width="0.1524" layer="91"/>
<pinref part="U1" gate="U$1" pin="VCC@6"/>
<wire x1="66.04" y1="55.88" x2="63.5" y2="55.88" width="0.1524" layer="91"/>
<wire x1="63.5" y1="55.88" x2="63.5" y2="58.42" width="0.1524" layer="91"/>
<pinref part="U1" gate="U$1" pin="VCC@4"/>
<wire x1="63.5" y1="58.42" x2="63.5" y2="60.96" width="0.1524" layer="91"/>
<wire x1="66.04" y1="58.42" x2="63.5" y2="58.42" width="0.1524" layer="91"/>
<junction x="63.5" y="58.42"/>
<junction x="63.5" y="60.96"/>
<wire x1="63.5" y1="60.96" x2="53.34" y2="60.96" width="0.1524" layer="91"/>
<junction x="53.34" y="60.96"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="VCC"/>
<wire x1="17.78" y1="17.78" x2="25.4" y2="17.78" width="0.1524" layer="91"/>
<pinref part="P+3" gate="VCC" pin="VCC"/>
<wire x1="25.4" y1="17.78" x2="25.4" y2="22.86" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R1" gate="G$1" pin="2"/>
<pinref part="P+4" gate="VCC" pin="VCC"/>
<wire x1="20.32" y1="66.04" x2="20.32" y2="68.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="S3" gate="G$1" pin="2"/>
<pinref part="P+1" gate="VCC" pin="VCC"/>
<wire x1="116.84" y1="111.76" x2="124.46" y2="111.76" width="0.1524" layer="91"/>
<wire x1="124.46" y1="111.76" x2="124.46" y2="114.3" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="170.18" y1="127" x2="157.48" y2="127" width="0.1524" layer="91"/>
<pinref part="P+5" gate="VCC" pin="VCC"/>
<wire x1="157.48" y1="127" x2="157.48" y2="129.54" width="0.1524" layer="91"/>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="170.18" y1="127" x2="170.18" y2="121.92" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="JP1" gate="A" pin="1"/>
<wire x1="162.56" y1="38.1" x2="144.78" y2="38.1" width="0.1524" layer="91"/>
<pinref part="P+9" gate="VCC" pin="VCC"/>
<wire x1="144.78" y1="38.1" x2="144.78" y2="40.64" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="JP1" gate="A" pin="2"/>
<wire x1="170.18" y1="38.1" x2="187.96" y2="38.1" width="0.1524" layer="91"/>
<pinref part="P+8" gate="VCC" pin="VCC"/>
<wire x1="187.96" y1="38.1" x2="187.96" y2="40.64" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$6" gate="G$1" pin="VREF"/>
<wire x1="218.44" y1="45.72" x2="218.44" y2="38.1" width="0.1524" layer="91"/>
<wire x1="218.44" y1="38.1" x2="213.36" y2="38.1" width="0.1524" layer="91"/>
<pinref part="P+7" gate="VCC" pin="VCC"/>
<wire x1="213.36" y1="38.1" x2="213.36" y2="40.64" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J6" gate="G$1" pin="1"/>
<wire x1="167.64" y1="15.24" x2="152.4" y2="15.24" width="0.1524" layer="91"/>
<pinref part="P+10" gate="VCC" pin="VCC"/>
<wire x1="152.4" y1="15.24" x2="152.4" y2="17.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SCK" class="0">
<segment>
<pinref part="U1" gate="U$1" pin="PB5(SCK)"/>
<wire x1="114.3" y1="7.62" x2="124.46" y2="7.62" width="0.1524" layer="91"/>
<label x="116.84" y="7.62" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="SCK"/>
<wire x1="10.16" y1="15.24" x2="0" y2="15.24" width="0.1524" layer="91"/>
<label x="0" y="15.24" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="182.88" y1="134.62" x2="170.18" y2="134.62" width="0.1524" layer="91"/>
<label x="172.72" y="134.62" size="1.778" layer="95"/>
<pinref part="R4" gate="G$1" pin="2"/>
<wire x1="182.88" y1="121.92" x2="182.88" y2="134.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="MISO" class="0">
<segment>
<pinref part="U1" gate="U$1" pin="PB4(MISO)"/>
<wire x1="114.3" y1="10.16" x2="124.46" y2="10.16" width="0.1524" layer="91"/>
<label x="116.84" y="10.16" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="MISO"/>
<wire x1="10.16" y1="17.78" x2="0" y2="17.78" width="0.1524" layer="91"/>
<label x="0" y="17.78" size="1.778" layer="95"/>
</segment>
</net>
<net name="MOSI" class="0">
<segment>
<pinref part="U1" gate="U$1" pin="PB3(MOSI/OC2)"/>
<wire x1="114.3" y1="12.7" x2="124.46" y2="12.7" width="0.1524" layer="91"/>
<label x="116.84" y="12.7" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="MOSI"/>
<wire x1="17.78" y1="15.24" x2="27.94" y2="15.24" width="0.1524" layer="91"/>
<label x="22.86" y="15.24" size="1.778" layer="95"/>
</segment>
</net>
<net name="RST" class="0">
<segment>
<pinref part="U1" gate="U$1" pin="PC6(/RESET)"/>
<wire x1="66.04" y1="66.04" x2="58.42" y2="66.04" width="0.1524" layer="91"/>
<label x="58.42" y="66.04" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="RST"/>
<wire x1="10.16" y1="12.7" x2="0" y2="12.7" width="0.1524" layer="91"/>
<label x="0" y="12.7" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="20.32" y1="52.28" x2="20.32" y2="53.34" width="0.1524" layer="91"/>
<pinref part="C7" gate="&gt;NAME" pin="2"/>
<wire x1="20.32" y1="53.34" x2="20.32" y2="55.88" width="0.1524" layer="91"/>
<wire x1="17.78" y1="53.34" x2="20.32" y2="53.34" width="0.1524" layer="91"/>
<junction x="20.32" y="53.34"/>
<wire x1="20.32" y1="53.34" x2="27.94" y2="53.34" width="0.1524" layer="91"/>
<label x="22.86" y="53.34" size="1.778" layer="95"/>
<pinref part="S2" gate="G$1" pin="PIN3-4"/>
</segment>
</net>
<net name="RTS#" class="0">
<segment>
<pinref part="C7" gate="&gt;NAME" pin="1"/>
<wire x1="10.16" y1="53.34" x2="2.54" y2="53.34" width="0.1524" layer="91"/>
<label x="2.54" y="53.34" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="RTS"/>
<wire x1="160.02" y1="53.34" x2="149.86" y2="53.34" width="0.1524" layer="91"/>
<label x="149.86" y="53.34" size="1.778" layer="95"/>
</segment>
</net>
<net name="RX" class="0">
<segment>
<pinref part="U1" gate="U$1" pin="PD0(RXD)"/>
<wire x1="114.3" y1="43.18" x2="124.46" y2="43.18" width="0.1524" layer="91"/>
<label x="119.38" y="43.18" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="TXD"/>
<wire x1="160.02" y1="58.42" x2="149.86" y2="58.42" width="0.1524" layer="91"/>
<label x="149.86" y="58.42" size="1.778" layer="95"/>
</segment>
</net>
<net name="TX" class="0">
<segment>
<pinref part="U1" gate="U$1" pin="PD1(TXD)"/>
<wire x1="114.3" y1="40.64" x2="124.46" y2="40.64" width="0.1524" layer="91"/>
<label x="119.38" y="40.64" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="RXD"/>
<wire x1="160.02" y1="55.88" x2="149.86" y2="55.88" width="0.1524" layer="91"/>
<label x="149.86" y="55.88" size="1.778" layer="95"/>
</segment>
</net>
<net name="VCC-5-REG" class="0">
<segment>
<pinref part="J4" gate="G$1" pin="VOUT"/>
<wire x1="86.36" y1="109.58" x2="96.52" y2="109.58" width="0.1524" layer="91"/>
<pinref part="C2" gate="&gt;NAME" pin="2"/>
<wire x1="91.44" y1="106.68" x2="91.44" y2="109.58" width="0.1524" layer="91"/>
<wire x1="91.44" y1="109.58" x2="86.36" y2="109.58" width="0.1524" layer="91"/>
<junction x="86.36" y="109.58"/>
<wire x1="101.6" y1="109.58" x2="86.36" y2="109.58" width="0.1524" layer="91"/>
<label x="86.36" y="111.76" size="1.778" layer="95"/>
<pinref part="S3" gate="G$1" pin="1"/>
<wire x1="86.36" y1="109.58" x2="106.68" y2="109.58" width="0.1524" layer="91"/>
<wire x1="106.68" y1="109.58" x2="106.68" y2="109.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="U$3" gate="G$1" pin="C"/>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="157.48" y1="109.22" x2="157.48" y2="106.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="U$5" gate="G$1" pin="A"/>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="182.88" y1="106.68" x2="182.88" y2="111.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VCC-12" class="0">
<segment>
<pinref part="J4" gate="G$1" pin="VIN"/>
<wire x1="63.5" y1="109.48" x2="55.88" y2="109.48" width="0.1524" layer="91"/>
<pinref part="C1" gate="&gt;NAME" pin="2"/>
<wire x1="60.96" y1="106.68" x2="60.96" y2="109.48" width="0.1524" layer="91"/>
<wire x1="60.96" y1="109.48" x2="63.5" y2="109.48" width="0.1524" layer="91"/>
<junction x="63.5" y="109.48"/>
<label x="55.88" y="111.76" size="1.778" layer="95"/>
<pinref part="J3" gate="G$1" pin="2"/>
<wire x1="50.8" y1="114.3" x2="50.8" y2="109.48" width="0.1524" layer="91"/>
<wire x1="50.8" y1="109.48" x2="63.5" y2="109.48" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$3" gate="G$1" pin="A"/>
<wire x1="157.48" y1="116.84" x2="157.48" y2="119.38" width="0.1524" layer="91"/>
<wire x1="157.48" y1="119.38" x2="144.78" y2="119.38" width="0.1524" layer="91"/>
<label x="144.78" y="119.38" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$6" gate="G$1" pin="VBB"/>
<wire x1="246.38" y1="45.72" x2="248.92" y2="45.72" width="0.1524" layer="91"/>
<pinref part="C8" gate="&gt;NAME" pin="2"/>
<wire x1="248.92" y1="45.72" x2="248.92" y2="38.1" width="0.1524" layer="91"/>
<pinref part="C9" gate="&gt;NAME" pin="2"/>
<wire x1="259.08" y1="38.1" x2="248.92" y2="38.1" width="0.1524" layer="91"/>
<junction x="248.92" y="38.1"/>
<wire x1="259.08" y1="38.1" x2="269.24" y2="38.1" width="0.1524" layer="91"/>
<junction x="259.08" y="38.1"/>
<label x="259.08" y="38.1" size="1.778" layer="95"/>
</segment>
</net>
<net name="VCC-5-FTDI" class="0">
<segment>
<pinref part="S3" gate="G$1" pin="3"/>
<wire x1="106.68" y1="114.3" x2="93.98" y2="114.3" width="0.1524" layer="91"/>
<label x="91.44" y="116.84" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="VCC"/>
<wire x1="160.02" y1="60.96" x2="144.78" y2="60.96" width="0.1524" layer="91"/>
<label x="144.78" y="60.96" size="1.778" layer="95"/>
</segment>
</net>
<net name="ENC-1" class="0">
<segment>
<pinref part="U1" gate="U$1" pin="PD3(INT1)"/>
<wire x1="114.3" y1="35.56" x2="124.46" y2="35.56" width="0.1524" layer="91"/>
<label x="116.84" y="35.56" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="JP1" gate="A" pin="5"/>
<wire x1="162.56" y1="33.02" x2="149.86" y2="33.02" width="0.1524" layer="91"/>
<label x="149.86" y="33.02" size="1.778" layer="95"/>
</segment>
</net>
<net name="ENC-2" class="0">
<segment>
<pinref part="U1" gate="U$1" pin="PD2(INT0)"/>
<wire x1="114.3" y1="38.1" x2="124.46" y2="38.1" width="0.1524" layer="91"/>
<label x="116.84" y="38.1" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="JP1" gate="A" pin="6"/>
<wire x1="170.18" y1="33.02" x2="182.88" y2="33.02" width="0.1524" layer="91"/>
<label x="175.26" y="33.02" size="1.778" layer="95"/>
</segment>
</net>
<net name="MOT-IN1" class="0">
<segment>
<pinref part="U1" gate="U$1" pin="PD5(T1)"/>
<wire x1="114.3" y1="30.48" x2="124.46" y2="30.48" width="0.1524" layer="91"/>
<label x="116.84" y="30.48" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$6" gate="G$1" pin="IN1"/>
<wire x1="218.44" y1="48.26" x2="210.82" y2="48.26" width="0.1524" layer="91"/>
<label x="208.28" y="48.26" size="1.778" layer="95"/>
</segment>
</net>
<net name="SW-TX" class="0">
<segment>
<pinref part="U1" gate="U$1" pin="PD6(AIN0)"/>
<wire x1="114.3" y1="27.94" x2="124.46" y2="27.94" width="0.1524" layer="91"/>
<label x="116.84" y="27.94" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J6" gate="G$1" pin="4"/>
<wire x1="167.64" y1="7.62" x2="157.48" y2="7.62" width="0.1524" layer="91"/>
<label x="157.48" y="7.62" size="1.778" layer="95"/>
</segment>
</net>
<net name="SW-RX" class="0">
<segment>
<pinref part="U1" gate="U$1" pin="PD7(AIN1)"/>
<wire x1="114.3" y1="25.4" x2="124.46" y2="25.4" width="0.1524" layer="91"/>
<label x="116.84" y="25.4" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J6" gate="G$1" pin="3"/>
<wire x1="167.64" y1="10.16" x2="157.48" y2="10.16" width="0.1524" layer="91"/>
<label x="157.48" y="10.16" size="1.778" layer="95"/>
</segment>
</net>
<net name="MOT-OUT2" class="0">
<segment>
<pinref part="U$6" gate="G$1" pin="OUT2"/>
<wire x1="246.38" y1="53.34" x2="256.54" y2="53.34" width="0.1524" layer="91"/>
<label x="246.38" y="53.34" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J5" gate="G$1" pin="1"/>
<wire x1="233.68" y1="22.86" x2="220.98" y2="22.86" width="0.1524" layer="91"/>
<label x="215.9" y="22.86" size="1.778" layer="95"/>
</segment>
</net>
<net name="MOT-OUT1" class="0">
<segment>
<pinref part="U$6" gate="G$1" pin="OUT1"/>
<wire x1="246.38" y1="48.26" x2="256.54" y2="48.26" width="0.1524" layer="91"/>
<label x="246.38" y="48.26" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J5" gate="G$1" pin="2"/>
<wire x1="233.68" y1="20.32" x2="220.98" y2="20.32" width="0.1524" layer="91"/>
<label x="215.9" y="20.32" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="R3" gate="G$1" pin="1"/>
<pinref part="U$4" gate="G$1" pin="A"/>
<wire x1="170.18" y1="111.76" x2="170.18" y2="106.68" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
