/*
 Caja Reductora (Gearbox) 

 Measures input and output rotational speeds of a gearbox counting pulses from a pair of 
 MOCH22A Optointerrupter used as encoders (non-quadrature). Then sends them, as well as 
 the time needed to count them, trhough Bluetooth to an app that converts pulses into 
 RPM.
 Also produces a PWM signal in order to regulate the gearbox's DC motor. Its duty cycle 
 is set by the user in the app. On start, it defaults to 128.
 Periodically, it sends the readings to the app. That period of time is defined in 
 'interval' variable.
 The encoders' pulses are counted using interruptions, whose functions are 
 'countEncInput()' and 'countEncOutput()'.

 Pins used:
 - 2: Encoder 1, gearbox's input
 - 3: Encoder 2, gearbox's output
 - 5: PWM signal
 - 10: Software serial's RX, Bluetooth's TX
 - 11: Software serial's TX, Bluetooth's RX

Copyright (C) 2018 Daniel Marquina
 */
 
#include <SoftwareSerial.h>

// System variables
unsigned long interval = 2000; // ms
unsigned long previousMillis = 0;

// Bluetooth communication variables
char c;
String pwmStr;
SoftwareSerial BlueT(7, 6); // RX (Bluetooth TX, old:10), TX (Bluetooth RX, old:11)

// Motor Variables
const byte motorPin = 5;
int pwm = 128;

// Encoders variables
const byte inputIntPin = 3; // old:2
const byte outputIntPin = 2; // old:3
unsigned long encPulsesInput = 0;
unsigned long encPulsesOutput = 0;
unsigned long last_input_interrupt_time = 0;
unsigned long last_output_interrupt_time = 0;
int last_input_state = 0;
int last_output_state = 0;

// Debugging variables
unsigned long startTime = 0;
unsigned long endTime = 0;

void setup(){
  // Serial communication initialization
  Serial.begin(9600);
  Serial.println("Caja reductora initialized!");
  BlueT.begin(9600);
  
  // Attach interrupts
  pinMode(inputIntPin, INPUT_PULLUP);
  pinMode(outputIntPin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(inputIntPin), countEncInput, CHANGE );
  attachInterrupt(digitalPinToInterrupt(outputIntPin), countEncOutput, CHANGE );

  // Motor PWM
  pinMode(motorPin, OUTPUT);
  analogWrite(motorPin,pwm); 
}

void loop(){
  unsigned long currentMillis = millis();
   if ((unsigned long)(currentMillis - previousMillis) >= interval){
    // Time to execute
    // First, detachInterrupts
    detachInterrupt(digitalPinToInterrupt(inputIntPin));
    detachInterrupt(digitalPinToInterrupt(outputIntPin));
    
    // Second, send sensors' readings
    sendEncoders(currentMillis - previousMillis);
    
    // Third, check whether a pwm value has been set from GUI 
    receivePWM();
    
    // Last, restart counters and re-attach interrupts 
    encPulsesInput = 0;
    encPulsesOutput = 0;
    last_input_state = digitalRead(inputIntPin);
    last_output_state = digitalRead(outputIntPin);
    attachInterrupt(digitalPinToInterrupt(inputIntPin), countEncInput, CHANGE );
    attachInterrupt(digitalPinToInterrupt(outputIntPin), countEncOutput, CHANGE );
    
    previousMillis = millis();
   }  
}

void receivePWM(){
  // Read incoming messages, lasts circa 180 ms with debugging code
  if (BlueT.available() > 0){
    c = BlueT.read();
    if (c == '#'){
      startTime = millis();
      Serial.println("========================================");
      Serial.print("Starts at (ms): ");
      Serial.println(startTime);
      //read protocol, engage in loop
      while(c != '%'){
        // acumulate in a string
        if (BlueT.available() > 0){
          c = BlueT.read();
          if (c == '%'){
            break;
          }
          pwmStr += c;
        }
        Serial.print("Received character: ");
        Serial.println(c);
      }
      // end of communication
      Serial.print("PWM value: ");
      Serial.println(pwmStr);
      pwm = pwmStr.toInt();
      if (pwm >= 0 && pwm <= 255){
        analogWrite(motorPin,pwm);              
      }
      pwmStr = "";
      Serial.print("Ends at (ms): ");
      endTime = millis();
      Serial.println(endTime);
      Serial.print("Lasts (ms): ");
      Serial.println(endTime - startTime);
    }
  }  
}

void sendEncoders(unsigned long dt){
  // Debugging
  startTime = millis();
  Serial.println("========================================");
  Serial.print("Starts at (ms): ");
  Serial.println(startTime);
  // Format is #(encoder1)$(encoder2)$(dt)%
  BlueT.print('#');
  BlueT.print(encPulsesInput); // encPulsesInput 
  BlueT.print('$');  
  BlueT.print(encPulsesOutput); // encPulsesOutput 
  BlueT.print('$');
  BlueT.print(dt);
  BlueT.print('%');
  // Debugging 
  Serial.print("Input encoder's counts: ");
  Serial.println(encPulsesInput);
  Serial.print("Output encoder's counts: ");
  Serial.println(encPulsesOutput);
  Serial.print("Time (ms): ");
  Serial.println(dt);
  endTime = millis();
  Serial.print("Ends at (ms): ");
  Serial.println(endTime);
  Serial.print("Lasts (ms): ");
  Serial.println(endTime - startTime);
}

void countEncInput(){
  //++encPulsesInput;
  unsigned long interrupt_time = millis();
  int current_input_state = digitalRead(inputIntPin);
    if (current_input_state != last_input_state){
      ++encPulsesInput;
      last_input_state = current_input_state;      
    }
}

void countEncOutput(){
  unsigned long interrupt_time = millis();
  int current_output_state = digitalRead(outputIntPin);
    if (current_output_state != last_output_state){
      ++encPulsesOutput;
      last_output_state = current_output_state;      
    }
}

