package pe.edu.tecsup.fablab.caja_reductora_gui;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.util.Locale;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {

    // Speed variables
    int progressChangedValue = 0;
    double gearRatio = 0.037037037;
    double inputSpeed = 0;
    double inputPR = 20;
    double idealSpeed = 0;
    double outputSpeed = 0;
    double outputPR = 120;
    public String inputS = "Input speed: ";
    String idealS = "Ideal speed: ";
    String outputS = "Output speed: ";
    String unitsS = " RPM";

    // Layout elements
    SeekBar speedBar;
    TextView inputT, idealT, outputT;
    Button connectButton;
    String connectButtonState;

    // Bluetooth variables
    String macAddress;
    BluetoothAdapter mBluetoothAdapter;
    BluetoothDevice mBluetoothDevice;
    private final static int PERMISSION_CONSTANT = 13;
    private final static int REQUEST_ENABLE_BT = 11;
    private ConnectThread mConnectThread;
    private ConnectedThread mConnectedThread;
    private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    BlueHandler mHandler = new BlueHandler(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.i("BlueT", "It creates.");

        // Initiate  views
        speedBar = findViewById(R.id.seekBar);
        speedBar.setEnabled(false);
        inputT = findViewById(R.id.inputTextView);
        idealT = findViewById(R.id.idealTextView);
        outputT = findViewById(R.id.outputTextView);
        connectButton = findViewById(R.id.connectButton);

        // Setting up Bluetooth
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        // Check whether this device supports Bluetooth or not
        if (mBluetoothAdapter == null) {
            connectButton.setEnabled(false);
            Toast.makeText(this, "This device does not support Bluetooth.",
                    Toast.LENGTH_SHORT).show();
        }

        // Setting up Seek bar
        speedBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progressChangedValue = progress;
            }
            public void onStartTrackingTouch(SeekBar seekBar) {
                // Do nothing, for now
            }
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (mConnectedThread != null){
                    byte[] b = ("#" + String.valueOf(progressChangedValue) + "%").getBytes();
                    mConnectedThread.write(b);
                }
                else{
                    Log.i("BlueT", "No ConnectedThread.");
                }
            }
        });
    }

    @Override
    protected void onStart(){
        super.onStart();
        Log.i("BlueT", "It starts.");

        // Enable Bluetooth if disabled
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }

        // Permission has to be requested in order to perform startDiscovery()
        if(ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED){
            Log.i("BlueT", "Permission granted.");
        }else{
            Log.i("BlueT", "Permission denied.");
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_COARSE_LOCATION)){}
            else{
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                        PERMISSION_CONSTANT);
            }
        }
    }

    @Override
    protected void onResume(){
        super.onResume();
        Log.i("BlueT", "It resumes.");
    }

    @Override
    protected void onStop(){
        //should destroy threads
        super.onStop();
        Log.i("BlueT", "It stops.");
        if (mConnectedThread != null){
            mConnectedThread.cancel();
            mConnectedThread = null;
        }
        if (mConnectThread != null){
            mConnectThread.cancel();
            mConnectThread = null;
        }
        connectButtonState = "Connect to device";
        connectButton.setText(connectButtonState);
        connectButton.setEnabled(true);
        speedBar.setEnabled(false);
    }

    @Override
    protected void onRestart(){
        super.onRestart();
        Log.i("BlueT", "It restarts.");

        /*restartFlag = true;
        if (macAddress != null){
            mBluetoothDevice = mBluetoothAdapter.getRemoteDevice(macAddress);
            //Should restart threads
            if (mConnectThread == null){
                mConnectThread = new ConnectThread(mBluetoothDevice);
                mConnectThread.start();
            }
        }
        else{
            Log.i("BlueT", "No MAC address registered.");
        }*/
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i("BlueT", "It destroys.");
    }

    // This method is started when connectButton is pressed
    public void connectToBluetooth(View v){
        Intent deviceIntent = new Intent(this, ChooseDeviceActivity.class);
        startActivityForResult(deviceIntent, 1);
    }

    // This method updates displayed speeds
    public void updateSpeeds(String message){
        // Format is #(encoder1)$(encoder2)$(dt)%
        Log.i("BlueT", "It runs.");
        double inputP, outputP, dt;
        inputP = Double.parseDouble(message.substring(0, message.indexOf("$")));
        message = message.substring(message.indexOf("$") + 1);
        outputP = Double.parseDouble(message.substring(0, message.indexOf("$")));
        message = message.substring(message.indexOf("$") + 1);
        dt = Double.parseDouble(message);
        inputSpeed =  (inputP * 60000.0) / (dt * inputPR * 2.0); //inputP * 1000.0 / dt; // For Hz
        idealSpeed = inputSpeed * gearRatio;
        outputSpeed = (outputP * 60000.0) / (dt * outputPR * 2.0); //outputP * 1000.0 / dt; // For Hz
        inputT.setText(inputS.concat(String.format(Locale.getDefault(), "%.1f", inputSpeed)).concat(unitsS));
        idealT.setText(idealS.concat(String.format(Locale.getDefault(),"%.1f", idealSpeed)).concat(unitsS));
        outputT.setText(outputS.concat(String.format(Locale.getDefault(),"%.1f", outputSpeed)).concat(unitsS));
    }

    // This method receives Bluetooth device's MAC address
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if(resultCode == RESULT_OK){
                connectButtonState = "Connecting...";
                connectButton.setText(connectButtonState);
                connectButton.setEnabled(false);

                macAddress = data.getStringExtra("DeviceAddress");
                mBluetoothDevice = mBluetoothAdapter.getRemoteDevice(macAddress);
                mBluetoothAdapter.cancelDiscovery();
                if (mConnectThread != null){
                    mConnectThread.cancel();
                    mConnectThread = null;
                }
                mConnectThread = new ConnectThread(mBluetoothDevice);
                mConnectThread.start();
            }
            if (resultCode == RESULT_CANCELED){
                Toast.makeText(this, "Device could not be found.",
                        Toast.LENGTH_LONG).show();
            }
        }
    }

    // Thread to handle connection
    private class ConnectThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final BluetoothDevice mmDevice;

        public ConnectThread(BluetoothDevice device) {
            Log.i("BlueT", "Thread initialized");
            // Use a temporary object that is later assigned to mmSocket,
            // because mmSocket is final
            BluetoothSocket tmp = null;
            mmDevice = device;

            // Get a BluetoothSocket to connect with the given BluetoothDevice
            try {
                // MY_UUID is the app's UUID string, also used by the server code
                tmp = mmDevice.createInsecureRfcommSocketToServiceRecord(MY_UUID);
                Log.i("BlueT", "Socket created.");
                //mHandler.obtainMessage(12).sendToTarget();
            } catch (IOException e) {
                Log.i("BlueT", "Socket could not be created");
                //mHandler.obtainMessage(13).sendToTarget();
            }
            mmSocket = tmp;
        }

        public void run() {
            // Cancel discovery because it will slow down the connection
            //mBluetoothAdapter.cancelDiscovery();

            try {
                // Connect the device through the socket. This will block
                // until it succeeds or throws an exception
                mmSocket.connect();
                Log.i("BlueT", "Socket connected.");
                mHandler.obtainMessage(12).sendToTarget();
            } catch (IOException connectException) {
                // Unable to connect; close the socket and get out
                Log.i("BlueT", "Socket unable to connect");
                try {
                    mmSocket.close();
                    Log.i("BlueT", "Socket closed.");
                } catch (IOException closeException) { }
                mHandler.obtainMessage(13).sendToTarget();
                return;
            }

            // Do work to manage the connection (in a separate thread)
            mConnectedThread = new ConnectedThread(mmSocket);
            mConnectedThread.start();
        }

        // Will cancel an in-progress connection, and close the socket
        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) { }
        }
    }

    // Thread to handle communication
    private class ConnectedThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;

        public ConnectedThread(BluetoothSocket socket) {
            Log.i("BlueT", "Connected thread successfully created.");
            mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            // Get the input and output streams, using temp objects because
            // member streams are final
            try {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e) { }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        public void run() {
            byte[] buffer = new byte[1024];  // buffer store for the stream
            int bytes; // bytes returned from read()

            // Keep listening to the InputStream until an exception occurs
            while (true) {
                Log.i("BlueT", "Thread is alive.");
                try {
                    // Read from the InputStream
                    bytes = mmInStream.read(buffer);
                    // Send the obtained bytes to the UI activity
                    // TODO name 'what' parameter, currently means 56 LOL
                    mHandler.obtainMessage(11, bytes, -1, buffer)
                            .sendToTarget();
                } catch (IOException e) {
                    mHandler.obtainMessage(13).sendToTarget();
                    break;
                }
            }
        }

        /* Call this from the main activity to send data to the remote device */
        public void write(byte[] bytes) {
            try {
                mmOutStream.write(bytes);
            } catch (IOException e) { }
        }

        /* Call this from the main activity to shutdown the connection */
        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) { }
        }
    }

    // Handler that passes information from ConnectedThread to MainActivity
    private static class BlueHandler extends Handler{
        private final WeakReference<MainActivity> mainActivityWeakReference;
        String readMessage;
        String completeMessage = "";
        int index, index2;
        boolean flag = false;

        public BlueHandler(MainActivity mainActivity){
            mainActivityWeakReference = new WeakReference<>(mainActivity);
        }

        @Override
        public void handleMessage(Message msg){
            MainActivity mActivity = mainActivityWeakReference.get();
            switch (msg.what){
                // TODO: name 'what' parameter
                case 11: {
                    byte[] readBuf = (byte[]) msg.obj;
                    // construct a string from the valid bytes in the buffer
                    readMessage = new String(readBuf, 0, msg.arg1);
                    Log.i("BlueT", "Received: " + readMessage);
                    index = readMessage.lastIndexOf("#");
                    if (index > -1) {
                        readMessage = readMessage.substring(index);
                        flag = true;
                    }
                    if (flag) {
                        completeMessage += readMessage;
                        index2 = completeMessage.indexOf("%");
                        if (index2 > -1) {
                            completeMessage = completeMessage.substring(1, index2);
                            int count0 = countStringOccurrences(completeMessage, "#");
                            int count2 = countStringOccurrences(completeMessage, "$");
                            if (count0 == 0 && count2 == 2) {
                                // TODO: erase following code
                                Log.i("BlueT", "Message: " + completeMessage);
                                mActivity.updateSpeeds(completeMessage);
                            } else {
                                Log.i("BlueT", "Error found in message.");
                            }
                            flag = false;
                            completeMessage = "";
                        }
                    }
                    break;
                }
                case 12: { // Device successfully connected
                    Log.i("BlueT", "Case 12");
                    mActivity.connectButtonState = " Connected to " + mActivity.mBluetoothDevice.getName();
                    mActivity.connectButton.setText(mActivity.connectButtonState);
                    mActivity.connectButton.setEnabled(false);
                    mActivity.speedBar.setEnabled(true);
                    break;
                }
                case 13: { // Disconnect device, reset UI
                    Log.i("BlueT", "Case 13");
                    mActivity.connectButtonState = "Connect to device";
                    mActivity.connectButton.setText(mActivity.connectButtonState);
                    mActivity.connectButton.setEnabled(true);
                    mActivity.speedBar.setEnabled(false);
                    //TODO: reset sensor labels too
                }
            }
        }
    }

    /**
     * Count the number of instances of substring within a string.
     *
     * @param string     String to look for substring in.
     * @param substring  Sub-string to look for.
     * @return           Count of substrings in string.
     */
    public static int countStringOccurrences(final String string, final String substring)
    {
        int count = 0;
        int idx = 0;

        while ((idx = string.indexOf(substring, idx)) != -1)
        {
            idx++;
            count++;
        }

        return count;
    }
}
