package pe.edu.tecsup.fablab.caja_reductora_gui;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Set;

public class ChooseDeviceActivity extends AppCompatActivity {
    // Bluetooth variables
    private boolean discoveryFlag = true;
    BluetoothAdapter mBluetoothAdapter;
    BluetoothDevice mBluetoothDevice;
    private final static int REQUEST_ENABLE_BT = 11;

    // ListView variables
    private ArrayAdapter<String> mNewDevicesArrayAdapter;

    // Layout variables
    ListView mListView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_device);

        // Set up ListView
        mNewDevicesArrayAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1);
        mListView = (ListView) findViewById(R.id.lvBlueT);
        mListView.setAdapter(mNewDevicesArrayAdapter);
        mListView.setOnItemClickListener(mDeviceListener);

        // Register for broadcasts when a device is discovered and when discovery is finished.
        IntentFilter blueTFilter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        blueTFilter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        this.registerReceiver(mReceiver, blueTFilter);

        // Bluetooth adapter initialization
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        // Enable Bluetooth if disabled
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }

        // Consult synchronized devices
        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
        // If there are paired devices
        if (pairedDevices.size() > 0) {
            // Loop through paired devices
            for (BluetoothDevice device : pairedDevices) {
                String pairedDeviceName = device.getName();
                String pairedDeviceHardwareAddress = device.getAddress(); // MAC address
                // Add devices to ListView
                mNewDevicesArrayAdapter.add(pairedDeviceName + "\n" + pairedDeviceHardwareAddress);
            }
        }

        // Start discovery
        if (mBluetoothAdapter.isDiscovering()){
            mBluetoothAdapter.cancelDiscovery();
        }
        mBluetoothAdapter.startDiscovery();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // Don't forget to unregister the ACTION_FOUND receiver.
        unregisterReceiver(mReceiver);
        mBluetoothAdapter.cancelDiscovery();
    }

    // Create a BroadcastReceiver for ACTION_FOUND and ACTION_DISCOVERY_FINISHED.
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                // Discovery has found a device. Get the BluetoothDevice object and its info from
                // the Intent.
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                String newDeviceName = device.getName();
                String newDeviceHardwareAddress = device.getAddress(); // MAC address
                if (newDeviceName != null){
                    // Add to listView
                    mNewDevicesArrayAdapter.add(newDeviceName + "\n" + newDeviceHardwareAddress);
                }
            }
            if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)){
                // Restart discovery. It is cancelled by OnDestroy and OnItemClickListener.
                if (discoveryFlag){
                    mBluetoothAdapter.startDiscovery();
                }else {
                    mBluetoothAdapter.cancelDiscovery();
                }
            }
            if (BluetoothDevice.ACTION_BOND_STATE_CHANGED.equals(action)){
                Log.i("BlueT", "Wooooooow.");
            }
        }
    };


    // To return MAC address use this function
    private AdapterView.OnItemClickListener mDeviceListener = new AdapterView.OnItemClickListener(){
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            discoveryFlag = false;
            mBluetoothAdapter.cancelDiscovery();
            String deviceName = ((TextView) view).getText().toString();
            String macAddress = deviceName.substring(deviceName.length() - 17);
            Intent getDevice = new Intent();
            getDevice.putExtra("DeviceAddress", macAddress);
            mBluetoothDevice = mBluetoothAdapter.getRemoteDevice(macAddress);
            int state = mBluetoothDevice.getBondState();
            if (state == BluetoothDevice.BOND_NONE){
                try {
                    mBluetoothDevice.createBond();
                    for (int i = 0; i < 100; i++){
                        if (mBluetoothDevice.getBondState() == BluetoothDevice.BOND_BONDING) {
                            Log.i("BlueT", "Bonding device...");
                            break;
                        }
                    }
                    while (mBluetoothDevice.getBondState() != BluetoothDevice.BOND_BONDED){
                        if (mBluetoothDevice.getBondState() == BluetoothDevice.BOND_NONE){
                            Log.i("BlueT", "Bond could not be created");
                            setResult(RESULT_CANCELED, getDevice);
                            break;
                        }
                    }
                    if (mBluetoothDevice.getBondState() ==  BluetoothDevice.BOND_BONDED){
                        Log.i("BlueT", "Device bonded.");
                        setResult(RESULT_OK, getDevice);
                    }
                } catch (Exception e){
                    Log.i("BlueT", "Bond could not be created");
                    e.printStackTrace();
                    setResult(RESULT_CANCELED, getDevice);
                    return;
                }
            }else{
                Log.i("BlueT", "Device already bonded (paired).");
                setResult(RESULT_OK, getDevice);
            }
            finish();
        }
    };
}
